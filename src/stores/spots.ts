import { action, observable } from 'mobx';
import { Spot, ISpotApi, PointGeojson } from '../models/spot';
import { Http } from './http';
import { RootStore } from '.';
import { Sector } from '../models/sector';

class SpotStore {
    public rootStore: RootStore;

    public currentSpotIndex: number;

    public isEditingSpot: boolean;

    @observable
    public currentSpot: Spot | null;

    @observable
    public currentSpotList: Spot[];

    private http: Http;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.http = new Http();
        this.currentSpotIndex = -1;
        this.isEditingSpot = false;
        this.currentSpot = null;
        this.currentSpotList = [];
    }

    @action
    public reset() {
        this.currentSpotIndex = -1;
        this.isEditingSpot = false;
        this.currentSpot = null;
        this.currentSpotList = [];
    }

    @action
    private setCurrentSpot(spot: Spot | null) {
        this.currentSpot = spot;
        this.rootStore.mapStore.loadSpotMarker();
    }

    @action
    private setCurrentSpotList(spotList: Spot[]) {
        this.currentSpotList = spotList;
    }

    @action
    public setCurrentSpotIndex(index: number) {
        this.currentSpotIndex = index;
    }

    @action
    public updateCurrentSpot(spot: Spot | null) {
        if (spot) {
            for (let i = 0; i < this.currentSpotList.length; i++) {
                if (spot.id === this.currentSpotList[i].id) {
                    this.setCurrentSpot(this.currentSpotList[i]);
                    this.rootStore.appStore.setSpotSelectedIndex(spot.id);
                    break;
                }
            }
        } else {
            this.setCurrentSpot(null);
            this.rootStore.appStore.setSpotSelectedIndex(-1);
        }
    }

    public updateSpotList() {
        if (this.rootStore.sectorStore.currentSector) {
            this.setCurrentSpotList(
                this.rootStore.sectorStore.currentSector.spotSet
            );
        } else {
            this.setCurrentSpotList([]);
        }
    }

    public createNewSpot = async (name: string, point: PointGeojson) => {
        try {
            const payload = {
                name: name,
                point: point,
                sectorId: this.rootStore.sectorStore.currentSector!.id
            };
            const res = await this.http.axios.post<ISpotApi>(
                'tuners/spots/',
                payload
            );
            const fetchedSpot = new Spot(res.data);
            const currentSector = new Sector(
                this.rootStore.sectorStore.currentSector!
            );
            await this.rootStore.areaStore.getAreaList();
            this.rootStore.areaStore.updateCurrentArea(
                this.rootStore.areaStore.currentArea
            );
            this.rootStore.sectorStore.updateSectorList();
            this.rootStore.sectorStore.updateCurrentSector(currentSector);
            this.updateSpotList();
            this.updateCurrentSpot(fetchedSpot);
            return true;
        } catch (error) {
            console.log(error.response.data);
            return false;
        }
    };

    public deleteSpot = async (spot: Spot | null) => {
        if (spot === null) {
            return false;
        }
        try {
            const res = await this.http.axios.delete(`tuners/spots/${spot.id}`);
            if (res.status === 204) {
                const currentSector = new Sector(
                    this.rootStore.sectorStore.currentSector!
                );
                await this.rootStore.areaStore.getAreaList();
                this.rootStore.areaStore.updateCurrentArea(
                    this.rootStore.areaStore.currentArea
                );
                this.rootStore.sectorStore.updateSectorList();
                this.rootStore.sectorStore.updateCurrentSector(currentSector);
                this.updateSpotList();
                this.updateCurrentSpot(null);
                return true;
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    };

    public patchSpot = async (payload: Object) => {
        if (this.currentSpot === null) {
            return false;
        }

        try {
            const res = await this.http.axios.patch<ISpotApi>(
                `tuners/spots/${this.currentSpot.id}`,
                payload
            );
            const fetchedSpot = new Spot(res.data);
            const currentSector = new Sector(
                this.rootStore.sectorStore.currentSector!
            );
            await this.rootStore.areaStore.getAreaList();
            this.rootStore.areaStore.updateCurrentArea(
                this.rootStore.areaStore.currentArea
            );
            this.rootStore.sectorStore.updateSectorList();
            this.rootStore.sectorStore.updateCurrentSector(currentSector);
            this.updateSpotList();
            this.updateCurrentSpot(fetchedSpot);
            return true;
        } catch (error) {
            console.log(error.response.data);
            return false;
        }
    };
}

export { SpotStore };
