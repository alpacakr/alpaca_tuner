import { configure } from 'mobx';
import { UserStore } from './users';
import { AppStore } from './app';
import { SectorStore } from './sectors';
import { AreaStore } from './areas';
import { MapStore } from './map';
import { PointStore } from './point';
import { SpotStore } from './spots';
import { HandlerStore } from './handlers';

configure({
    enforceActions: 'observed'
});

export interface IBaseStore {
    rootStore: RootStore;
}

class RootStore {
    public userStore: UserStore;
    public appStore: AppStore;
    public sectorStore: SectorStore;
    public areaStore: AreaStore;
    public mapStore: MapStore;
    public pointStore: PointStore;
    public spotStore: SpotStore;
    public handlerStore: HandlerStore;

    constructor() {
        this.userStore = new UserStore();
        this.appStore = new AppStore();
        this.sectorStore = new SectorStore(this);
        this.areaStore = new AreaStore(this);
        this.mapStore = new MapStore(this);
        this.pointStore = new PointStore(this);
        this.spotStore = new SpotStore(this);
        this.handlerStore = new HandlerStore(this);
    }

    public reset() {
        this.appStore.reset();
        this.sectorStore.reset();
        this.areaStore.reset();
        this.mapStore.reset();
        this.pointStore.reset();
        this.spotStore.reset();
    }
}

const STORE_NAME = 'store';

export interface IStoreInjectProps {
    [STORE_NAME]?: RootStore;
}

export { RootStore, STORE_NAME };
