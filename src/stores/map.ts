import { observable, action } from 'mobx';
import { ReactInstance } from 'react';
import { Area } from '../models/area';
import { Sector } from '../models/sector';
import markerBlack from '../img/marker_black.png';
import markerBlue from '../img/marker_blue.png';
import markerAdd from '../img/markerAdd.png';
import { RootStore } from '.';
import { PointSet } from '../utils/PointSet';

interface PolygonListItem {
    id: number;
    polygon: naver.maps.Polygon;
}

class MapStore {
    public rootStore: RootStore;

    @observable
    public map: naver.maps.KVOArrayOfCoords = undefined;

    @observable
    public areaPolygonList: PolygonListItem[];

    @observable
    public sectorPolygonList: PolygonListItem[];

    @observable
    public pointCircleList: naver.maps.Marker[];

    @observable
    public supportPointCircleList: naver.maps.Marker[];

    @observable
    public supportPolylineList: naver.maps.Polyline[];

    @observable
    public currentPolygon: PolygonListItem | null;

    @observable
    public currentListener: naver.maps.MapEventListener | null;

    @observable
    public spotMarkerList: naver.maps.Marker[];

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.areaPolygonList = [];
        this.sectorPolygonList = [];
        this.pointCircleList = [];
        this.supportPointCircleList = [];
        this.supportPolylineList = [];
        this.currentPolygon = null;
        this.currentListener = null;
        this.spotMarkerList = [];
    }

    @action
    public reset() {
        this.areaPolygonList = [];
        this.sectorPolygonList = [];
        this.pointCircleList = [];
        this.supportPointCircleList = [];
        this.supportPolylineList = [];
        this.currentPolygon = null;
        this.currentListener = null;
        this.spotMarkerList = [];
    }

    @action
    public reloadAllInstance() {
        this.loadAreaPolygon(this.rootStore.areaStore.areaList);
        if (this.rootStore.areaStore.currentArea) {
            this.loadSectorPolygon(
                this.rootStore.areaStore.currentArea.sectorSet
            );
        } else {
            this.loadSectorPolygon([]);
        }
        this.loadPointCircle(this.rootStore.pointStore.pointList);
        if (this.currentPolygon) {
            if (this.currentPolygon.polygon) {
                this.currentPolygon.polygon.setMap(null);
            }
        }
        this.currentPolygon = null;
    }

    @action
    public loadAreaPolygon = (areaList: Area[]) => {
        for (let i = 0; i < this.areaPolygonList.length; i++) {
            const item = this.areaPolygonList[i];
            if (item.polygon) {
                item.polygon.setMap(null);
            }
        }
        this.areaPolygonList = [];
        for (let i = 0; i < areaList.length; i++) {
            const areaPolygonListItem = {
                id: areaList[i].id,
                polygon: new window.naver.maps.Polygon({
                    map: this.map,
                    paths: [],
                    strokeColor: 'darkorange',
                    strokeWeight: 3,
                    strokeStyle: 'shortdash',
                    fillColor: 'darkorange',
                    fillOpacity: 0,
                    zIndex: 1
                })
            };
            if (areaList[i].polygon) {
                const area = areaList[i];
                const path = area.polygon!.coordinates[0].map(
                    point => new window.naver.maps.Point(point[0], point[1])
                );
                areaPolygonListItem.polygon.setPaths(path);
            }
            /*
            areaPolygonListItem.polygon.addListener(
                'mouseover',
                this.changePolygonFillOpacity(i, 'area', 0.5)
            );
            areaPolygonListItem.polygon.addListener(
                'mouseout',
                this.changePolygonFillOpacity(i, 'area', 0)
            );
            */
            this.areaPolygonList.push(areaPolygonListItem);
        }
    };

    @action
    public loadSectorPolygon(sectorList: Sector[]) {
        for (let i = 0; i < this.sectorPolygonList.length; i++) {
            const item = this.sectorPolygonList[i];
            if (item.polygon) {
                item.polygon.setMap(null);
            }
        }
        this.sectorPolygonList = [];
        for (let i = 0; i < sectorList.length; i++) {
            if (sectorList[i].polygon) {
                const sectorPolygonListItem = {
                    id: sectorList[i].id,
                    polygon: new window.naver.maps.Polygon({
                        map: this.map,
                        paths: sectorList[i].polygon!.coordinates[0].map(
                            coord =>
                                new window.naver.maps.Point(coord[0], coord[1])
                        ),
                        strokeColor: 'royalblue',
                        strokeWeight: 3,
                        fillColor: 'royalblue',
                        fillOpacity: 0,
                        zIndex: 2
                    })
                };
                this.sectorPolygonList.push(sectorPolygonListItem);
            } else {
                const sectorPolygonListItem = {
                    id: sectorList[i].id,
                    polygon: new window.naver.maps.Polygon({
                        map: this.map,
                        paths: [],
                        strokeColor: 'royalblue',
                        strokeWeight: 3,
                        fillColor: 'royalblue',
                        fillOpacity: 0
                    })
                };
                this.sectorPolygonList.push(sectorPolygonListItem);
            }
        }
    }

    @action
    public changePolygonFillOpacity = (
        index: number,
        type: 'area' | 'sector',
        opacity: number
    ) => () => {
        let list: PolygonListItem[] = [];
        if (type === 'area') {
            list = this.areaPolygonList;
        } else if (type === 'sector') {
            list = this.sectorPolygonList;
        } else {
            return;
        }
        if (index < list.length) {
            list[index].polygon.setOptions('fillOpacity', opacity);
        }
    };

    @action
    public changeAllSectorFillOpacity = (opacity: number) => () => {
        for (let i = 0; i < this.sectorPolygonList.length; i++) {
            this.sectorPolygonList[i].polygon.setOptions(
                'fillOpacity',
                opacity
            );
        }
    };

    /*//////////////////////////////////////////////////////////////////////////
// 여기부터는 pointCircle (marker) 에 관련된 method 입니다.
//////////////////////////////////////////////////////////////////////////*/

    private makePointCircle(index: number, point: [number, number]) {
        const pointCircleListItem = new window.naver.maps.Marker({
            position: new window.naver.maps.Point(point[0], point[1]),
            icon: markerBlack,
            map: this.map,
            title: `${index}`,
            zIndex: 20
        });
        window.naver.maps.Event.addListener(
            pointCircleListItem,
            'click',
            this.rootStore.pointStore.onEditPoint(index)
        );
        window.naver.maps.Event.addListener(
            pointCircleListItem,
            'mouseover',
            this.changeOnePointCircleColor(index, markerBlue)
        );
        window.naver.maps.Event.addListener(
            pointCircleListItem,
            'mouseout',
            this.changeOnePointCircleColor(index, markerBlack)
        );
        if (index < this.pointCircleList.length) {
            this.pointCircleList[index].setMap(null);
            this.pointCircleList[index] = pointCircleListItem;
        } else {
            this.pointCircleList.push(pointCircleListItem);
        }
    }

    @action
    public deletePointCircle(index: number) {
        if (index < this.pointCircleList.length) {
            this.pointCircleList[index].setMap(null);
            this.pointCircleList.splice(index, 1);
            this.reloadCurrentPolygon();
        }
    }

    @action
    public loadPointCircle(pointList: [number, number][]) {
        this.clearSupportPointCircleList();
        this.clearSupportPolylineList();
        for (let i = 0; i < this.pointCircleList.length; i++) {
            this.pointCircleList[i].setMap(null);
        }
        this.pointCircleList = [];
        for (let i = 0; i < pointList.length; i++) {
            this.makePointCircle(i, pointList[i]);
        }
    }

    @action
    public addOnePointCircle(point: [number, number]) {
        this.makePointCircle(this.pointCircleList.length, point);
        this.reloadCurrentPolygon();
    }

    @action
    public editOnePointCircle(index: number, point: [number, number]) {
        this.makePointCircle(index, point);
        this.reloadCurrentPolygon();
    }

    @action
    public changeOnePointCircleColor = (
        index: number,
        colorUrl: string
    ) => () => {
        this.pointCircleList[index].setIcon({
            url: colorUrl
        });
    };

    /*//////////////////////////////////////////////////////////////////////////
// 여기부터는 supportPointCircle 에 관련된 method 입니다.
//////////////////////////////////////////////////////////////////////////*/

    @action
    public loadSupportPointCircleList() {
        let tempSet = new PointSet();
        for (let i = 0; i < this.areaPolygonList.length; i++) {
            const areaPolygon = this.areaPolygonList[i];
            if (
                this.rootStore.areaStore.currentArea &&
                areaPolygon.id === this.rootStore.areaStore.currentArea.id &&
                !this.rootStore.sectorStore.currentSector
            ) {
                continue;
            }
            let paths = areaPolygon.polygon.getPath();
            if (paths) {
                for (let j = 0; j < paths.length; j++) {
                    tempSet.add(paths.getAt(j));
                }
            }
        }
        for (let p = 0; p < this.sectorPolygonList.length; p++) {
            const sectorPolygon = this.sectorPolygonList[p];
            if (
                this.rootStore.sectorStore.currentSector &&
                sectorPolygon.id === this.rootStore.sectorStore.currentSector.id
            ) {
                continue;
            }
            let paths = sectorPolygon.polygon.getPath();
            if (paths) {
                for (let q = 0; q < paths.length; q++) {
                    tempSet.add(paths.getAt(q));
                }
            }
        }
        for (let z = 0; z < tempSet.length; z++) {
            const point = new window.naver.maps.Point(
                tempSet.getAt(z)!['x'],
                tempSet.getAt(z)!['y']
            );
            const pointCircleListIem = new window.naver.maps.Marker({
                position: point,
                map: this.map,
                icon: {
                    url: markerAdd,
                    origin: new window.naver.maps.Point(0, 0),
                    anchor: new window.naver.maps.Point(8, 8)
                },
                zIndex: 10
            });
            window.naver.maps.Event.addListener(
                pointCircleListIem,
                'click',
                this.clickSupportPointCircleHandler(z)
            );
            this.supportPointCircleList.push(pointCircleListIem);
        }
    }

    @action
    public clearSupportPointCircleList() {
        for (let i = 0; i < this.supportPointCircleList.length; i++) {
            this.supportPointCircleList[i].setMap(null);
        }
        this.supportPointCircleList = [];
    }

    public clickSupportPointCircleHandler = (index: number) => () => {
        const clickedPoint = this.supportPointCircleList[index];
        const newPoint: [number, number] = [
            clickedPoint.getPosition()['x'],
            clickedPoint.getPosition()['y']
        ];
        let currentEditIndex = this.rootStore.pointStore.currentEditPointIndex;
        if (currentEditIndex === -1) {
            this.rootStore.pointStore.addNewPointInList(newPoint);
            this.addOnePointCircle(newPoint);
        } else {
            this.rootStore.pointStore.editPoint(newPoint);
            this.editOnePointCircle(currentEditIndex, newPoint);
            this.clearListenersFromMap();
            this.addListenerToMap(
                'click',
                this.rootStore.pointStore.clickForAddNewPointListener
            );
            window.naver.maps.Event.resumeDispatch(
                this.pointCircleList[currentEditIndex],
                'mouseout'
            );
        }
    };

    /*//////////////////////////////////////////////////////////////////////////
// 여기부터는 supportPolyline 에 관련된 method 입니다.
//////////////////////////////////////////////////////////////////////////*/
    @action
    public loadSupportPolylineList() {
        for (let i = 0; i < this.areaPolygonList.length; i++) {
            const areaPolygon = this.areaPolygonList[i];
            if (
                this.rootStore.areaStore.currentArea &&
                areaPolygon.id === this.rootStore.areaStore.currentArea.id &&
                !this.rootStore.sectorStore.currentSector
            ) {
                continue;
            }
            let paths = areaPolygon.polygon.getPath();
            if (paths) {
                for (let j = 0; j < paths.length - 1; j++) {
                    let firstPoint = new window.naver.maps.Point(
                        paths.getAt(j)['x'],
                        paths.getAt(j)['y']
                    );
                    let secondPoint = new window.naver.maps.Point(
                        paths.getAt(j + 1)['x'],
                        paths.getAt(j + 1)['y']
                    );
                    const polyLineListItem = new window.naver.maps.Polyline({
                        map: this.map,
                        path: [firstPoint, secondPoint],
                        strokeColor: 'green',
                        strokeWeight: 10,
                        strokeOpacity: 0,
                        zIndex: 5,
                        clickable: true
                    });
                    this.supportPolylineList.push(polyLineListItem);
                }
            }
        }
        for (let p = 0; p < this.sectorPolygonList.length; p++) {
            const sectorPolygon = this.sectorPolygonList[p];
            if (
                this.rootStore.sectorStore.currentSector &&
                sectorPolygon.id === this.rootStore.sectorStore.currentSector.id
            ) {
                continue;
            }
            let paths = sectorPolygon.polygon.getPath();
            if (paths) {
                for (let q = 0; q < paths.length - 1; q++) {
                    let firstPoint = new window.naver.maps.Point(
                        paths.getAt(q)
                    );
                    let secondPoint = new window.naver.maps.Point(
                        paths.getAt(q + 1)
                    );
                    const polyLineListItem = new window.naver.maps.Polyline({
                        map: this.map,
                        path: [firstPoint, secondPoint],
                        strokeColor: 'green',
                        strokeWeight: 10,
                        strokeOpacity: 0,
                        zIndex: 5,
                        clickable: true
                    });
                    this.supportPolylineList.push(polyLineListItem);
                }
            }
        }
        for (let z = 0; z < this.supportPolylineList.length; z++) {
            const polyLineListItem = this.supportPolylineList[z];
            window.naver.maps.Event.addListener(
                polyLineListItem,
                'mouseover',
                this.changeOnePolylineOpacity(z, 0.7)
            );
            window.naver.maps.Event.addListener(
                polyLineListItem,
                'mouseout',
                this.changeOnePolylineOpacity(z, 0)
            );
            window.naver.maps.Event.addListener(
                polyLineListItem,
                'click',
                this.clickSupportPolylineHandler(z)
            );
        }
    }

    @action
    public clearSupportPolylineList() {
        for (let i = 0; i < this.supportPolylineList.length; i++) {
            this.supportPolylineList[i].setMap(null);
        }
        this.supportPolylineList = [];
    }

    @action
    public changeOnePolylineOpacity = (
        index: number,
        opacity: number
    ) => () => {
        this.supportPolylineList[index].setOptions('strokeOpacity', opacity);
    };

    @action
    public clickSupportPolylineHandler = (index: number) => (
        e: naver.maps.PointerEvent
    ) => {
        const clickedPolyline = this.supportPolylineList[index];
        const clickedPolylinePath = clickedPolyline.getPath();
        let a = new window.naver.maps.Point(clickedPolylinePath.getAt(0));
        let b = new window.naver.maps.Point(clickedPolylinePath.getAt(1));
        let c = new window.naver.maps.Point(e.coord);
        const newPoint: [number, number] = this.getClosestPointOnLine(a, b, c);

        let currentEditIndex = this.rootStore.pointStore.currentEditPointIndex;
        if (currentEditIndex === -1) {
            this.rootStore.pointStore.addNewPointInList(newPoint);
            this.addOnePointCircle(newPoint);
        } else {
            this.rootStore.pointStore.editPoint(newPoint);
            this.editOnePointCircle(currentEditIndex, newPoint);
            this.clearListenersFromMap();
            this.addListenerToMap(
                'click',
                this.rootStore.pointStore.clickForAddNewPointListener
            );
            window.naver.maps.Event.resumeDispatch(
                this.pointCircleList[currentEditIndex],
                'mouseout'
            );
        }
    };

    public getClosestPointOnLine(
        a: naver.maps.Point,
        b: naver.maps.Point,
        p: naver.maps.Point
    ) {
        let atob = { x: b['x'] - a['x'], y: b['y'] - a['y'] };
        let atop = { x: p['x'] - a['x'], y: p['y'] - a['y'] };
        let len = atob.x * atob.x + atob.y * atob.y;
        let dot = atop.x * atob.x + atop.y * atob.y;
        let t = Math.min(1, Math.max(0, dot / len));

        let point: [number, number] = [
            a['x'] + atob.x * t,
            a['y'] + atob.y * t
        ];
        return point;
    }

    //////////////////////////////////////////////////////////////////////////////
    //////여기부터는 spotMarker 에 관련된 method 입니다.
    //////////////////////////////////////////////////////////////////////////////

    @action
    public makeSpotMarker(index: number, point: [number, number]) {
        const spot = this.rootStore.spotStore.currentSpotList[index];
        const spotMarkerListItem = new window.naver.maps.Marker({
            position: new window.naver.maps.Point(point[0], point[1]),
            icon: markerBlack,
            map: this.map,
            title: `${spot.name}`,
            zIndex: 20
        });
        window.naver.maps.Event.addListener(
            spotMarkerListItem,
            'mouseover',
            this.changeOneSpotMarkerColor(index, markerBlue)
        );
        window.naver.maps.Event.addListener(
            spotMarkerListItem,
            'mouseout',
            this.changeOneSpotMarkerColor(index, markerBlack)
        );
        if (index < this.spotMarkerList.length) {
            this.spotMarkerList[index].setMap(null);
            spotMarkerListItem.setIcon({ url: markerBlue });
            this.spotMarkerList[index] = spotMarkerListItem;
        } else {
            this.spotMarkerList.push(spotMarkerListItem);
        }
    }

    @action
    public editSpotMarker(index: number, point: [number, number]) {
        this.makeSpotMarker(index, point);
    }

    @action
    public loadSpotMarker() {
        for (let i = 0; i < this.spotMarkerList.length; i++) {
            this.spotMarkerList[i].setMap(null);
        }
        this.spotMarkerList = [];
        for (
            let i = 0;
            i < this.rootStore.spotStore.currentSpotList.length;
            i++
        ) {
            this.makeSpotMarker(
                i,
                this.rootStore.spotStore.currentSpotList[i].point.coordinates
            );
        }
    }

    @action
    public changeOneSpotMarkerColor = (
        index: number,
        colorUrl: string
    ) => () => {
        this.spotMarkerList[index].setIcon({
            url: colorUrl
        });
    };

    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    @action
    public setMap(target: HTMLDivElement | ReactInstance, option: object) {
        this.map = new window.naver.maps.Map(target, option);
    }

    @action
    public addListenerToMap = (eventName: string, listener: Function) => {
        this.currentListener = window.naver.maps.Event.addListener(
            this.map,
            eventName,
            listener
        );
        this.map.setCursor('crosshair');
    };

    @action
    public clearListenersFromMap() {
        window.naver.maps.Event.removeListener(this.currentListener);
        this.map.setCursor('open');
    }

    @action
    public updateCurrentPolygonArea(area: Area | null) {
        if (!area) {
            if (this.currentPolygon) {
                this.currentPolygon.polygon.setMap(null);
            }
            this.currentPolygon = null;
        } else {
            for (let i = 0; i < this.areaPolygonList.length; i++) {
                if (this.areaPolygonList[i].id === area.id) {
                    this.currentPolygon = this.areaPolygonList[i];
                    this.currentPolygon.polygon!.setOptions(
                        'strokeColor',
                        'crimson'
                    );
                    this.currentPolygon.polygon!.setOptions(
                        'strokeStyle',
                        'solid'
                    );
                    this.currentPolygon.polygon!.setOptions('zIndex', 3);
                    break;
                }
            }
        }
    }

    @action
    public updateCurrentPolygonSector(sector: Sector | null) {
        if (!sector) {
            if (this.currentPolygon) {
                this.currentPolygon.polygon.setMap(null);
            }
            this.currentPolygon = null;
        } else {
            for (let i = 0; i < this.sectorPolygonList.length; i++) {
                if (this.sectorPolygonList[i].id === sector.id) {
                    this.currentPolygon = this.sectorPolygonList[i];
                    this.currentPolygon.polygon!.setOptions(
                        'strokeColor',
                        'navy'
                    );
                    this.currentPolygon.polygon!.setOptions('zIndex', 3);
                    break;
                }
            }
        }
    }

    @action
    public reloadCurrentPolygon() {
        if (!this.currentPolygon) {
            return;
        } else {
            this.currentPolygon.polygon!.setPath(
                this.pointCircleList.map(
                    marker =>
                        new window.naver.maps.Point(
                            marker.getPosition()['x'],
                            marker.getPosition()['y']
                        )
                )
            );
        }
    }
}

export { MapStore };
