import { action, observable } from 'mobx';
import { Area, IAreaApi } from '../models/area';
import { Http } from './http';
import { RootStore } from '.';
import { sortListByNameArea, sortListByNameSector } from '../utils/sort';

class AreaStore {
    public rootStore: RootStore;

    @observable
    public currentArea: Area | null;

    @observable
    public areaList: Area[];

    private http: Http;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.http = new Http();
        this.currentArea = null;
        this.areaList = [];
    }

    @action
    public reset() {
        this.currentArea = null;
        this.areaList = [];
    }

    @action
    private setCurrentArea(area: Area | null) {
        this.currentArea = area;
        this.rootStore.sectorStore.updateSectorList();
        this.rootStore.sectorStore.updateCurrentSector(null);
        this.rootStore.pointStore.clearPointList();
        this.rootStore.mapStore.reloadAllInstance();
        this.rootStore.mapStore.clearListenersFromMap();
        this.rootStore.mapStore.updateCurrentPolygonArea(area);
    }

    @action
    private setAreaList(areaList: Area[]) {
        this.areaList = areaList;
    }

    @action
    public updateCurrentArea(area: Area | null) {
        if (area) {
            for (let i = 0; i < this.areaList.length; i++) {
                if (area.id === this.areaList[i].id) {
                    this.setCurrentArea(this.areaList[i]);
                    this.rootStore.appStore.setAreaSelectedIndex(area.id);
                    break;
                }
            }
        } else {
            this.setCurrentArea(null);
            this.rootStore.appStore.setAreaSelectedIndex(-1);
        }
    }

    public async getAreaList() {
        try {
            const res = await this.http.axios.get<IAreaApi[]>('tuners/areas/');
            const fetchedAreaList = res.data.map(result => {
                let area = new Area(result);
                let newSectorList = sortListByNameSector(area.sectorSet);
                area.sectorSet = newSectorList;
                return area;
            });
            const newAreaList = sortListByNameArea(fetchedAreaList);
            this.setAreaList(newAreaList);
            return true;
        } catch (error) {
            return false;
        }
    }

    public async createNewArea(name: string) {
        try {
            const res = await this.http.axios.post<IAreaApi>('tuners/areas/', {
                name: name
            });
            const fetchedArea = new Area(res.data);
            await this.getAreaList();
            this.updateCurrentArea(fetchedArea);
            return true;
        } catch (error) {
            return false;
        }
    }

    public deleteArea = async (area: Area | null) => {
        if (area === null) {
            return false;
        }
        try {
            const res = await this.http.axios.delete(`tuners/areas/${area.id}/`);
            if (res.status === 204) {
                await this.getAreaList();
                this.updateCurrentArea(null);
                return true;
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    };

    public saveArea = async (pointList?: [number, number][], name?: string) => {
        if (this.currentArea === null) {
            return false;
        }

        try {
            let payload = {};
            if (name) {
                payload = {
                    name: name
                };
            } else if (pointList) {
                const newPointList = pointList.slice();
                newPointList.push(pointList[0]);
                payload = {
                    polygon: {
                        type: 'Polygon',
                        coordinates: [newPointList]
                    }
                };
            } else {
                return false;
            }
            const res = await this.http.axios.patch<IAreaApi>(
                `tuners/areas/${this.currentArea.id}/`,
                payload
            );
            const fetchedArea = new Area(res.data);
            await this.getAreaList();
            this.updateCurrentArea(fetchedArea);
            return true;
        } catch (error) {
            console.log(error.response.data);
            return false;
        }
    };
}

export { AreaStore };
