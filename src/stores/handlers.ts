import { RootStore } from '.';

class HandlerStore {
    public rootStore: RootStore;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }
}

export { HandlerStore };
