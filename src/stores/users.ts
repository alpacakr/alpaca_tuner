import { action, observable } from 'mobx';
import { IUserApi, User } from '../models/user';
import { Http } from './http';

class UserStore {
    @observable
    public loginUser: User | null;

    private http: Http;

    constructor() {
        this.loginUser = null;
        this.http = new Http();
    }

    @action
    private setLoginUser(user: User | null) {
        this.loginUser = user;
    }

    private postLoginSuccess(user: User) {
        this.setLoginUser(user);
        localStorage.setItem('token', user.authToken);
        this.http.axios.defaults.headers.common.Authorization = `Token ${
            user.authToken
        }`;
    }

    public async requestTokenLogin() {
        const token = localStorage.getItem('token');
        if (!token) {
            return false;
        }
        try {
            const response = await this.http.axios.post<IUserApi>(
                '/users/token_login/',
                { token }
            );
            const user = new User(response.data);
            this.postLoginSuccess(user);
            return true;
        } catch (error) {
            return false;
        }
    }

    public async requestStaffEmailLogin(email: string, password: string) {
        try {
            const response = await this.http.axios.post<IUserApi>(
                '/users/staff_email_login/',
                {
                    email,
                    password
                }
            );
            const user = new User(response.data);
            this.postLoginSuccess(user);
            return true;
        } catch (err) {
            return false;
        }
    }

    public async logout() {
        this.http.axios.defaults.headers.common.Authorization = undefined;
        localStorage.removeItem('token');
        this.setLoginUser(null);
    }
}

export { UserStore };
