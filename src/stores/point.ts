import { action, observable } from 'mobx';
import { Area } from '../models/area';
import { Sector } from '../models/sector';
import { RootStore } from '.';
import markerBlue from '../img/marker_blue.png';

class PointStore {
    public rootStore: RootStore;

    @observable
    public pointList: [number, number][];

    @observable
    public currentEditPointIndex: number;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.pointList = [];
        this.currentEditPointIndex = -1;
    }

    @action
    public reset() {
        this.pointList = [];
        this.currentEditPointIndex = -1;
    }

    @action
    public updatePointList(pointList: [number, number][]) {
        this.pointList = pointList;
        this.currentEditPointIndex = -1;
    }

    @action
    public loadPointList(target: Area | Sector) {
        if (target.polygon) {
            this.pointList = target.polygon.coordinates[0].slice();
            this.pointList.pop();
            this.currentEditPointIndex = -1;
        } else {
            this.pointList = [];
        }
    }

    @action
    public addNewPointInList(point: [number, number]) {
        this.pointList.push(point);
    }

    @action
    public editPoint(point: [number, number]) {
        if (this.currentEditPointIndex === -1) {
            return;
        } else {
            this.pointList[this.currentEditPointIndex] = point;
            this.currentEditPointIndex = -1;
        }
    }

    @action
    public deletePoint(index: number) {
        if (index < this.pointList.length) {
            this.pointList.splice(index, 1);
        }
    }

    @action
    public clearPointList() {
        this.pointList = [];
        this.currentEditPointIndex = -1;
    }

    @action
    public setCurrentEditPointIndex(index: number) {
        this.currentEditPointIndex = index;
    }

    public onEditPoint = (index: number) => () => {
        if (this.currentEditPointIndex !== -1) {
            return;
        }
        window.naver.maps.Event.stopDispatch(
            this.rootStore.mapStore.pointCircleList[index],
            'mouseout'
        );
        this.rootStore.mapStore.clearListenersFromMap();
        this.setCurrentEditPointIndex(index);
        this.rootStore.mapStore.changeOnePointCircleColor(index, markerBlue);
        this.rootStore.mapStore.addListenerToMap(
            'click',
            this.clickForEditPointListener
        );
    };

    public clickForAddNewPointListener = (e: naver.maps.PointerEvent) => {
        this.addNewPointInList([e.coord['x'], e.coord['y']]);
        this.rootStore.mapStore.addOnePointCircle([e.coord['x'], e.coord['y']]);
    };

    public clickForEditPointListener = (e: naver.maps.PointerEvent) => {
        const index = this.currentEditPointIndex;
        const point: [number, number] = [e.coord['x'], e.coord['y']];
        this.editPoint(point);
        this.rootStore.mapStore.editOnePointCircle(index, point);
        this.rootStore.mapStore.clearListenersFromMap();
        this.rootStore.mapStore.addListenerToMap(
            'click',
            this.clickForAddNewPointListener
        );
        window.naver.maps.Event.resumeDispatch(
            this.rootStore.mapStore.pointCircleList[index],
            'mouseout'
        );
    };
}

export { PointStore };
