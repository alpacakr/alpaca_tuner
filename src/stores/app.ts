import { action, observable } from 'mobx';

class AppStore {
    public isCreatableStatus: boolean;

    @observable
    public areaSelectedIndex: number;

    @observable
    public sectorSelectedIndex: number;

    @observable
    public spotSelectedIndex: number;

    @observable
    public simpleModalContent: React.ReactNode | null;

    @observable
    public isSimpleModalOpen: boolean;

    @observable
    public currentSmallMenuBar: React.ReactNode | null;

    constructor() {
        this.isCreatableStatus = true;
        this.areaSelectedIndex = -1;
        this.sectorSelectedIndex = -1;
        this.spotSelectedIndex = -1;
        this.simpleModalContent = null;
        this.isSimpleModalOpen = false;
        this.currentSmallMenuBar = null;
    }

    @action
    public reset() {
        this.isCreatableStatus = true;
        this.areaSelectedIndex = -1;
        this.sectorSelectedIndex = -1;
        this.spotSelectedIndex = -1;
        this.simpleModalContent = null;
        this.isSimpleModalOpen = false;
        this.currentSmallMenuBar = null;
    }

    @action
    public setAreaSelectedIndex(index: number) {
        this.areaSelectedIndex = index;
    }

    @action
    public setSectorSelectedIndex(index: number) {
        this.sectorSelectedIndex = index;
    }

    @action
    public setSpotSelectedIndex(index: number) {
        this.spotSelectedIndex = index;
    }

    @action
    public openSimpleModal(content: React.ReactNode) {
        this.simpleModalContent = content;
        this.isSimpleModalOpen = true;
    }

    @action
    public closeSimpleModal() {
        this.isSimpleModalOpen = false;
        this.simpleModalContent = null;
    }

    @action
    public changeSmallMenuBar(target: React.ReactNode | null) {
        this.currentSmallMenuBar = target;
    }
}

export { AppStore };
