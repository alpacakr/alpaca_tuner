import { action, observable } from 'mobx';
import { Sector, ISectorApi } from '../models/sector';
import { Http } from './http';
import { RootStore } from '.';

class SectorStore {
    public rootStore: RootStore;

    @observable
    public currentSector: Sector | null;

    @observable
    public currentSectorList: Sector[];

    private http: Http;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.http = new Http();
        this.currentSector = null;
        this.currentSectorList = [];
    }

    @action
    public reset() {
        this.currentSector = null;
        this.currentSectorList = [];
    }

    @action
    private setCurrentSector(sector: Sector | null) {
        this.currentSector = sector;
        this.rootStore.spotStore.updateSpotList();
        this.rootStore.spotStore.updateCurrentSpot(null);
        this.rootStore.pointStore.clearPointList();
        this.rootStore.mapStore.reloadAllInstance();
        this.rootStore.mapStore.clearListenersFromMap();
        this.rootStore.mapStore.updateCurrentPolygonSector(sector);
    }

    @action
    private setCurrentSectorList(sectorList: Sector[]) {
        this.currentSectorList = sectorList;
    }

    public updateCurrentSector(sector: Sector | null) {
        if (sector) {
            for (let i = 0; i < this.currentSectorList.length; i++) {
                if (sector.id === this.currentSectorList[i].id) {
                    this.setCurrentSector(this.currentSectorList[i]);
                    this.rootStore.appStore.setSectorSelectedIndex(sector.id);
                    break;
                }
            }
        } else {
            this.setCurrentSector(null);
            this.rootStore.appStore.setSectorSelectedIndex(-1);
        }
    }

    public updateSectorList() {
        if (this.rootStore.areaStore.currentArea) {
            this.setCurrentSectorList(
                this.rootStore.areaStore.currentArea.sectorSet
            );
        } else {
            this.setCurrentSectorList([]);
        }
    }

    public async createNewSector(name: string) {
        try {
            const payload = {
                name: name,
                areaId: this.rootStore.areaStore.currentArea!.id
            };
            const res = await this.http.axios.post<ISectorApi>(
                'tuners/sectors/',
                payload
            );
            const fetchedSector = new Sector(res.data);
            await this.rootStore.areaStore.getAreaList();
            this.rootStore.areaStore.updateCurrentArea(
                this.rootStore.areaStore.currentArea
            );
            this.updateSectorList();
            this.updateCurrentSector(fetchedSector);
            return false;
        } catch (error) {
            return false;
        }
    }

    public deleteSector = async (sector: Sector | null) => {
        if (sector === null) {
            return false;
        }
        try {
            const res = await this.http.axios.delete(
                `tuners/sectors/${sector.id}/`
            );
            if (res.status === 204) {
                await this.rootStore.areaStore.getAreaList();
                this.rootStore.areaStore.updateCurrentArea(
                    this.rootStore.areaStore.currentArea
                );
                this.updateSectorList();
                this.updateCurrentSector(null);
                return true;
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    };

    public saveSector = async (
        pointList?: [number, number][],
        name?: string
    ) => {
        if (this.currentSector === null) {
            return false;
        }
        try {
            let payload = {};
            if (name) {
                payload = {
                    name: name
                };
            } else if (pointList) {
                const newPointList = pointList.slice();
                newPointList.push(pointList[0]);
                payload = {
                    polygon: {
                        type: 'Polygon',
                        coordinates: [newPointList]
                    }
                };
            } else {
                return false;
            }
            const res = await this.http.axios.patch<ISectorApi>(
                `tuners/sectors/${this.currentSector.id}/`,
                payload
            );
            const fetchedSector = new Sector(res.data);
            await this.rootStore.areaStore.getAreaList();
            this.rootStore.areaStore.updateCurrentArea(
                this.rootStore.areaStore.currentArea
            );
            this.updateSectorList();
            this.updateCurrentSector(fetchedSector);
            return true;
        } catch (err) {
            return false;
        }
    };

    public changeAlpacaRangeInSector = async (min: number, max: number) => {
        if (this.currentSector === null) {
            return false;
        }

        try {
            let payload = { minNumAlpaca: min, maxNumAlpaca: max };
            const res = await this.http.axios.patch<ISectorApi>(
                `tuners/sectors/${this.currentSector.id}`,
                payload
            );
            const fetchedSector = new Sector(res.data);
            await this.rootStore.areaStore.getAreaList();
            this.rootStore.areaStore.updateCurrentArea(
                this.rootStore.areaStore.currentArea
            );
            this.updateSectorList();
            this.updateCurrentSector(fetchedSector);
            return true;
        } catch (err) {
            return false;
        }
    };
}

export { SectorStore };
