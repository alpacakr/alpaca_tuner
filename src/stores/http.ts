import axios, { AxiosInstance } from 'axios';
import { ENV_CONSTANTS } from '../constants';

class Http {
    public axios: AxiosInstance;

    constructor() {
        this.axios = axios.create({
            baseURL: ENV_CONSTANTS.baseURL
        });
    }
}

export { Http };
