import { geoContains } from 'd3';
import { PolygonGeojson } from '../models/area';
import { PointGeojson } from '../models/spot';

function isContainsPoint(polygon: PolygonGeojson | null, point: PointGeojson) {
    if (!polygon) {
        return false;
    }
    return geoContains(polygon, point.coordinates);
}

export { isContainsPoint };
