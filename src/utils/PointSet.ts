class PointSet {
    public pointSet: naver.maps.Point[];

    constructor() {
        this.pointSet = [];
    }

    public has(point: naver.maps.Point) {
        for (let i = 0; i < this.pointSet.length; i++) {
            if (
                this.pointSet[i]['x'] === point['x'] &&
                this.pointSet[i]['y'] === point['y']
            ) {
                return true;
            }
        }
        return false;
    }

    public add(point: naver.maps.Point) {
        if (!this.has(point)) {
            this.pointSet.push(point);
        }
    }

    public getAt(index: number) {
        if (index < this.pointSet.length) {
            return this.pointSet[index];
        }
        return null;
    }

    get length() {
        return this.pointSet.length;
    }
}

export { PointSet };
