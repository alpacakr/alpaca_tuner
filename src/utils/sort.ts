import { Area } from '../models/area';
import { Sector } from '../models/sector';

function sortListByNameArea(list: Area[]) {
    const newList = list.sort(compareFunction);
    return newList;
}

function compareFunction(a: Area | Sector, b: Area | Sector) {
    return a.name.localeCompare(b.name);
}

function sortListByNameSector(list: Sector[]) {
    const newList = list.sort(compareFunction);
    return newList;
}

export { sortListByNameArea, sortListByNameSector };
