const isProduction = process.env.NODE_ENV === 'production';

const baseURL = isProduction
    ? 'https://alpaca.kr/api'
    : 'https://zoooo.alpaca.kr/api';

export const env = {
    baseURL
};
