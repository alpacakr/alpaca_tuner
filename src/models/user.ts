export interface IUserApi {
    readonly id: number;
    readonly username: string;
    readonly name: string;
    readonly authToken: string;
}

class User {
    public readonly id: number;
    public readonly username: string;
    public readonly name: string;
    public readonly authToken: string;

    constructor(userApi: IUserApi) {
        this.id = userApi.id;
        this.username = userApi.username;
        this.name = userApi.name;
        this.authToken = userApi.authToken;
    }
}

export interface ISimpleUserApi {
    readonly id: number;
    readonly phoneNumber: string;
}

class SimpleUser {
    public readonly id: number;
    public phoneNumber: string;
    constructor(simpleUserApi: ISimpleUserApi) {
        this.id = simpleUserApi.id;
        this.phoneNumber = simpleUserApi.phoneNumber;
    }
}

export { User, SimpleUser };
