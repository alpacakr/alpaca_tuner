export interface ISpotApi {
    readonly id: number;
    readonly name: string;
    readonly point: PointGeojson;
    readonly photo: string;
    readonly detail: string;
    readonly createdAt: string;
    readonly sectorId: number;
    readonly lastUpdatedAt: string;
    readonly lastUpdatedBy: number;
}

export interface PointGeojson {
    type: 'Point';
    coordinates: [number, number];
}

class Spot {
    public id: number;
    public name: string;
    public point: PointGeojson;
    public photo: string;
    public detail: string;
    public createdAt: string;
    public sectorId: number;
    public lastUpdatedAt: string;
    public lastUpdatedBy: number;

    constructor(iSpotApi: ISpotApi) {
        this.id = iSpotApi.id;
        this.name = iSpotApi.name;
        this.point = iSpotApi.point;
        this.photo = iSpotApi.photo;
        this.detail = iSpotApi.detail;
        this.createdAt = iSpotApi.createdAt;
        this.sectorId = iSpotApi.sectorId;
        this.lastUpdatedAt = iSpotApi.lastUpdatedAt;
        this.lastUpdatedBy = iSpotApi.lastUpdatedBy;
    }
}

export { Spot };
