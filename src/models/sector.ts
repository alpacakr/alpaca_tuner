import { Spot } from './spot';

export interface ISectorApi {
    readonly id: number;
    readonly name: string;
    readonly polygon: PolygonGeojson | null;
    readonly createdAt: string;
    readonly minNumAlpaca: number;
    readonly maxNumAlpaca: number;
    readonly areaId: number;
    readonly lastUpdatedAt: string;
    readonly lastUpdatedBy: number;
    readonly spotSet: Spot[];
}

export interface PolygonGeojson {
    type: 'Polygon';
    coordinates: [number, number][][];
}

class Sector {
    public id: number;
    public name: string;
    public polygon: PolygonGeojson | null;
    public createdAt: string;
    public minNumAlpaca: number;
    public maxNumAlpaca: number;
    public areaId: number;
    public lastUpdatedAt: string;
    public lastUpdatedBy: number;
    public spotSet: Spot[];

    constructor(iSectorApi: ISectorApi) {
        this.id = iSectorApi.id;
        this.name = iSectorApi.name;
        this.polygon = iSectorApi.polygon;
        this.createdAt = iSectorApi.createdAt;
        this.minNumAlpaca = iSectorApi.minNumAlpaca;
        this.maxNumAlpaca = iSectorApi.maxNumAlpaca;
        this.areaId = iSectorApi.areaId;
        this.lastUpdatedAt = iSectorApi.lastUpdatedAt;
        this.lastUpdatedBy = iSectorApi.lastUpdatedBy;
        this.spotSet = iSectorApi.spotSet;
    }
}

export { Sector };
