import { Sector } from './sector';

export interface IAreaApi {
    readonly id: number;
    readonly name: string;
    readonly polygon: PolygonGeojson | null;
    readonly createdAt: string;
    readonly lastUpdatedAt: string;
    readonly lastUpdatedBy: number;
    readonly sectorSet: Sector[];
}

export interface PolygonGeojson {
    type: 'Polygon';
    coordinates: [number, number][][];
}

class Area {
    public id: number;
    public name: string;
    public polygon: PolygonGeojson | null;
    public createdAt: string;
    public lastUpdatedAt: string;
    public lastUpdatedBy: number;
    public sectorSet: Sector[];

    constructor(iAreaApi: IAreaApi) {
        this.id = iAreaApi.id;
        this.name = iAreaApi.name;
        this.polygon = iAreaApi.polygon;
        this.createdAt = iAreaApi.createdAt;
        this.lastUpdatedAt = iAreaApi.lastUpdatedAt;
        this.lastUpdatedBy = iAreaApi.lastUpdatedBy;
        this.sectorSet = iAreaApi.sectorSet;
    }
}

export { Area };
