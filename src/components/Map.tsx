import React from 'react';
import { inject, observer } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';

interface IProps extends IStoreInjectProps {
    mapStyle: any;
    mapOption: any;
}

declare global {
    interface Window {
        naver: any;
    }
}

@inject(STORE_NAME)
@observer
class Map extends React.Component<IProps, {}> {
    componentDidMount() {
        this.props[STORE_NAME]!.mapStore.setMap(
            this.refs.map,
            this.props.mapOption
        );
    }

    public render() {
        return <div id="map" ref="map" style={this.props.mapStyle} />;
    }
}

export { Map };
