import React from 'react';
import Button from '@material-ui/core/Button';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { computed } from 'mobx';
import { SectorEditSMB } from './SectorEditSMB';
import { ChangeSectorName } from './ChangeSectorName';

const styles = (theme: Theme) =>
    createStyles({
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '10px'
        }
    });

type IProps = WithStyles<typeof styles> & IStoreInjectProps;

@inject(STORE_NAME)
@observer
class SectorBasicSMB extends React.Component<IProps, {}> {
    @computed
    public get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    public get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    public get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    public get pointStore() {
        return this.props[STORE_NAME]!.pointStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    private onEditName = () => {
        this.appStore.openSimpleModal(<ChangeSectorName />);
    };

    private onEdit = () => {
        this.mapStore.addListenerToMap(
            'click',
            this.pointStore.clickForAddNewPointListener
        );
        this.appStore.isCreatableStatus = false;
        this.appStore.changeSmallMenuBar(<SectorEditSMB />);
        this.pointStore.loadPointList(this.sectorStore.currentSector!);
        this.mapStore.loadPointCircle(this.pointStore.pointList);
        this.mapStore.loadSupportPointCircleList();
        this.mapStore.loadSupportPolylineList();
    };

    private onDelete = () => {
        this.appStore.changeSmallMenuBar(null);
        this.sectorStore.deleteSector(this.sectorStore.currentSector);
    };

    public render() {
        return (
            <div style={{ display: 'flex' }}>
                <div
                    style={{
                        width: '200px',
                        paddingTop: '10px',
                        lineHeight: '25px',
                        fontSize: '20px',
                        textAlign: 'center'
                    }}
                >
                    {`${this.areaStore.currentArea!.name}`}
                    <br />
                    {this.sectorStore.currentSector
                        ? `${this.sectorStore.currentSector.name}`
                        : ''}
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onEditName}>
                        이름변경
                    </Button>
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onEdit}>
                        편집
                    </Button>
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onDelete}>
                        삭제
                    </Button>
                </div>
            </div>
        );
    }
}

const SectorBasicSMBWithStyles = withStyles(styles)(SectorBasicSMB);
export { SectorBasicSMBWithStyles as SectorBasicSMB };
