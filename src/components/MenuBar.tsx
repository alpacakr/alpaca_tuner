import { Divider, Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject } from 'mobx-react';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { IStoreInjectProps, STORE_NAME } from '../stores/';

const styles = (theme: Theme) =>
    createStyles({
        container: {
            flex: '0 0 150px',
            height: '100%'
        },
        smallTitleWrapper: {
            color: 'white',
            fontSize: '0.7em',
            backgroundColor: theme.palette.primary.main,
            padding: '8px',
            textAlign: 'center'
        },
        logoutWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

type Props = IStoreInjectProps &
    RouteComponentProps &
    WithStyles<typeof styles>;

@inject(STORE_NAME)
class MenuBar extends React.Component<Props, {}> {
    private onClickLogout = () => {
        this.props[STORE_NAME]!.userStore.logout();
        this.props.history.push('/login');
    };

    /*
    private onClick = () => {
        this.props[STORE_NAME]!.reset();
    };
    */

    public render() {
        return (
            <Paper className={this.props.classes.container}>
                <div className={this.props.classes.smallTitleWrapper}>
                    알파카 조율기
                </div>
                <div className={this.props.classes.logoutWrapper}>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={this.onClickLogout}
                    >
                        로그아웃
                    </Button>
                </div>
                <Divider />
                <List component="nav" disablePadding={true}>
                    <Link to="/">
                        <ListItem button={true}>
                            <ListItemText primary="Area 편집기" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/sector-setting">
                        <ListItem button={true}>
                            <ListItemText primary="Sector 설정" />
                        </ListItem>
                    </Link>
                    <Divider />
                </List>
                <Divider />
            </Paper>
        );
    }
}

const MenuBarWithStyles = withRouter(withStyles(styles)(MenuBar));

export { MenuBarWithStyles as MenuBar };
