import React, { ChangeEvent, FormEvent } from 'react';
import { observable, action, computed } from 'mobx';
import { observer, inject } from 'mobx-react';
import { TextField, Button } from '@material-ui/core';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { PointGeojson } from '../models/spot';

const styles = (theme: Theme) =>
    createStyles({
        dialogWrapper: {
            width: '300px',
            height: '300px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        inputWrapper: {
            width: '280px',
            margin: '10px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        errorWrapper: {
            width: '300px',
            height: '50px',
            color: 'red',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        buttonWrapper: {
            width: '300px',
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        button: {
            margin: '10px'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {
    point: naver.maps.Point;
}

@inject(STORE_NAME)
@observer
class CreateNewSpot extends React.Component<IProps, {}> {
    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @observable
    public spotName: string = '';

    @observable
    public loading: boolean = false;

    @observable
    public showError: boolean = false;

    @action
    private setLoading(bool: boolean) {
        this.loading = bool;
    }

    @action
    private setShowError(bool: boolean) {
        this.showError = bool;
    }

    @action
    private setSpotName(spotName: string) {
        this.spotName = spotName;
    }

    private onClose = () => {
        this.appStore.isCreatableStatus = true;
        this.mapStore.clearListenersFromMap();
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
        this.spotStore.updateCurrentSpot(null);
        this.appStore.changeSmallMenuBar(null);
    };

    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setLoading(true);
        this.setShowError(false);

        this.setLoading(true);
        if (this.spotName === '') {
            this.setLoading(false);
            this.setShowError(true);
        } else {
            let pointGeoJson: PointGeojson = {
                type: 'Point',
                coordinates: [this.props.point['x'], this.props.point['y']]
            };
            await this.spotStore.createNewSpot(this.spotName, pointGeoJson);
            this.setLoading(false);
            this.setShowError(false);
            this.props[STORE_NAME]!.appStore.closeSimpleModal();
            this.appStore.isCreatableStatus = true;
            this.mapStore.clearListenersFromMap();
            this.spotStore.updateCurrentSpot(null);
            this.appStore.changeSmallMenuBar(null);
        }
    };

    private onChangeSpotName = (e: ChangeEvent<HTMLInputElement>) => {
        this.setSpotName(e.currentTarget.value);
    };

    public render() {
        return (
            <div className={this.props.classes.dialogWrapper}>
                <form onSubmit={this.onSubmit}>
                    <div className={this.props.classes.inputWrapper}>
                        <TextField
                            label="New Spot의 이름"
                            value={this.spotName}
                            onChange={this.onChangeSpotName}
                            autoFocus={true}
                        />
                    </div>
                    <div className={this.props.classes.errorWrapper}>
                        {this.showError ? (
                            <div>Spot 이름을 입력해주세요.</div>
                        ) : (
                            <span />
                        )}
                    </div>
                    <div className={this.props.classes.buttonWrapper}>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={this.loading}
                            className={this.props.classes.button}
                        >
                            저장
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.onClose}
                            className={this.props.classes.button}
                        >
                            취소
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const CreateNewSpotWithStyles = withStyles(styles)(CreateNewSpot);
export { CreateNewSpotWithStyles as CreateNewSpot };
