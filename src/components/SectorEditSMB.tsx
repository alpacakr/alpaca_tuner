import React from 'react';
import Button from '@material-ui/core/Button';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { computed } from 'mobx';
import { SectorBasicSMB } from './SectorBasicSMB';

const styles = (theme: Theme) =>
    createStyles({
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '10px'
        }
    });

type IProps = WithStyles<typeof styles> & IStoreInjectProps;

@inject(STORE_NAME)
@observer
class SectorEditSMB extends React.Component<IProps, {}> {
    @computed
    public get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    public get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    public get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    public get pointStore() {
        return this.props[STORE_NAME]!.pointStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    private onSave = () => {
        this.sectorStore.saveSector(this.pointStore.pointList);
        this.appStore.isCreatableStatus = true;
        this.appStore.changeSmallMenuBar(<SectorBasicSMB />);
    };

    private onCancel = () => {
        this.sectorStore.updateCurrentSector(this.sectorStore.currentSector);
        this.appStore.isCreatableStatus = true;
        this.appStore.changeSmallMenuBar(<SectorBasicSMB />);
    };

    public render() {
        return (
            <div style={{ display: 'flex' }}>
                <div
                    style={{
                        width: '200px',
                        paddingTop: '10px',
                        lineHeight: '25px',
                        fontSize: '20px',
                        textAlign: 'center'
                    }}
                >
                    {`${this.areaStore.currentArea!.name}`}
                    <br />
                    {`${this.sectorStore.currentSector!.name}`}
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onSave}>
                        저장
                    </Button>
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onCancel}>
                        취소
                    </Button>
                </div>
            </div>
        );
    }
}

const SectorEditSMBWithStyles = withStyles(styles)(SectorEditSMB);
export { SectorEditSMBWithStyles as SectorEditSMB };
