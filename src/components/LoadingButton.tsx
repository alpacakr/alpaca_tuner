import { Button, CircularProgress } from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';
import React from 'react';

interface IProps extends ButtonProps {
    loading: boolean;
}

const LoadingButton: React.SFC<IProps> = props => {
    const { loading, ...buttonProps } = props;
    return (
        <Button {...buttonProps}>
            {props.loading ? (
                <CircularProgress size={20} style={{ color: '#fff' }} />
            ) : (
                props.children
            )}
        </Button>
    );
};

export { LoadingButton };
