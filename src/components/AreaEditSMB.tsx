import React from 'react';
import Button from '@material-ui/core/Button';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { computed } from 'mobx';
import { AreaBasicSMB } from './AreaBasicSMB';

const styles = (theme: Theme) =>
    createStyles({
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '10px'
        }
    });

type IProps = WithStyles<typeof styles> & IStoreInjectProps;

@inject(STORE_NAME)
@observer
class AreaEditSMB extends React.Component<IProps, {}> {
    @computed
    public get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    public get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    public get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    public get pointStore() {
        return this.props[STORE_NAME]!.pointStore;
    }

    private onSave = () => {
        this.areaStore.saveArea(this.pointStore.pointList).then(() => {
            this.mapStore.clearListenersFromMap();
            this.appStore.isCreatableStatus = true;
            this.appStore.changeSmallMenuBar(<AreaBasicSMB />);
            this.pointStore.clearPointList();
            this.mapStore.reloadAllInstance();
            this.mapStore.updateCurrentPolygonArea(this.areaStore.currentArea!);
        });
    };

    private onCancel = () => {
        this.mapStore.clearListenersFromMap();
        this.appStore.isCreatableStatus = true;
        this.appStore.changeSmallMenuBar(<AreaBasicSMB />);
        this.pointStore.clearPointList();
        this.mapStore.reloadAllInstance();
        this.mapStore.updateCurrentPolygonArea(this.areaStore.currentArea!);
    };

    public render() {
        return (
            <div style={{ display: 'flex' }}>
                <div
                    style={{
                        width: '200px',
                        paddingTop: '10px',
                        lineHeight: '50px',
                        fontSize: '20px',
                        textAlign: 'center'
                    }}
                >
                    {`${this.areaStore.currentArea!.name}`}
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onSave}>
                        저장
                    </Button>
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onCancel}>
                        취소
                    </Button>
                </div>
            </div>
        );
    }
}

const AreaEditSMBWithStyles = withStyles(styles)(AreaEditSMB);
export { AreaEditSMBWithStyles as AreaEditSMB };
