import React from 'react';
import Button from '@material-ui/core/Button';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { computed } from 'mobx';
import { RangeSetting } from './RangeSetting';

const styles = (theme: Theme) =>
    createStyles({
        textWrapper: {
            width: '200px',
            paddingTop: '10px',
            lineHeight: '25px',
            fontSize: '20px',
            textAlign: 'center'
        },
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '10px'
        }
    });

type IProps = WithStyles<typeof styles> & IStoreInjectProps;

@inject(STORE_NAME)
@observer
class RangeBasicSMB extends React.Component<IProps, {}> {
    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    private onEdit = () => {
        this.appStore.openSimpleModal(<RangeSetting />);
    };

    public render() {
        return (
            <div style={{ display: 'flex' }}>
                <div className={this.props.classes.textWrapper}>
                    {`${this.areaStore.currentArea!.name}`}
                    <br />
                    {this.sectorStore.currentSector
                        ? `${this.sectorStore.currentSector.name}`
                        : ''}
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onEdit}>
                        Range 설정
                    </Button>
                </div>
                <div className={this.props.classes.textWrapper}>
                    {this.sectorStore.currentSector
                        ? `Min: ${
                              this.sectorStore.currentSector!.minNumAlpaca
                          }대`
                        : '-'}
                    <br />
                    {this.sectorStore.currentSector
                        ? `Max: ${
                              this.sectorStore.currentSector!.maxNumAlpaca
                          }대`
                        : '-'}
                </div>
                <div />
            </div>
        );
    }
}

const RangeBasicSMBWithStyles = withStyles(styles)(RangeBasicSMB);
export { RangeBasicSMBWithStyles as RangeBasicSMB };
