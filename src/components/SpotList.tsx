import React from 'react';
import { Divider, Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { computed } from 'mobx';
import { STORE_NAME, IStoreInjectProps } from '../stores';
import { SimpleMessageSMB } from './SimpleMessageSMB';
import { CreateNewSpot } from './CreateNewSpot';
import markerBlue from '../img/marker_blue.png';
import { SpotBasicSMB } from './SpotBasicSMB';
import { PointGeojson } from '../models/spot';
import { isContainsPoint } from '../utils/geo';

const styles = (theme: Theme) =>
    createStyles({
        container: {
            width: '200px',
            height: '100%'
        },
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {}

@inject(STORE_NAME)
@observer
class SpotList extends React.Component<IProps, {}> {
    @computed
    get selectedIndex() {
        return this.props[STORE_NAME]!.appStore.spotSelectedIndex;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    public clickForCreateNewSpotHandler = (e: naver.maps.PointerEvent) => {
        let point = new window.naver.maps.Point(e.coord);
        let pointGeoJson: PointGeojson = {
            type: 'Point',
            coordinates: [point['x'], point['y']]
        };
        if (
            isContainsPoint(
                this.sectorStore.currentSector!.polygon,
                pointGeoJson
            )
        ) {
            this.appStore.openSimpleModal(<CreateNewSpot point={point} />);
        } else {
            this.appStore.changeSmallMenuBar(
                <SimpleMessageSMB
                    message={'Sector를 벗어났습니다. 다시 찍어주세요.'}
                    color={'red'}
                />
            );
        }
    };

    public onCreateNewSpot = () => {
        if (this.sectorStore.currentSector && this.appStore.isCreatableStatus) {
            this.appStore.isCreatableStatus = false;
            this.mapStore.addListenerToMap(
                'click',
                this.clickForCreateNewSpotHandler
            );
            this.appStore.changeSmallMenuBar(
                <SimpleMessageSMB message={'Spot을 찍어주세요'} color={'red'} />
            );
        }
    };

    public onSelectSpot = (index: number) => () => {
        this.spotStore.setCurrentSpotIndex(index);
        this.spotStore.updateCurrentSpot(this.spotStore.currentSpotList[index]);
        window.naver.maps.Event.stopDispatch(
            this.mapStore.spotMarkerList[index],
            'mouseout'
        );
        this.mapStore.spotMarkerList[index].setIcon({ url: markerBlue });
        this.appStore.changeSmallMenuBar(<SpotBasicSMB />);
    };

    public render() {
        for (let i = 0; i < this.mapStore.spotMarkerList.length; i++) {
            let spotMarker = this.mapStore.spotMarkerList[i];
            spotMarker.clearListeners('click');
            spotMarker.addListener('click', this.onSelectSpot(i));
        }
        return (
            <Paper className={this.props.classes.container}>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onCreateNewSpot}>
                        New Spot
                    </Button>
                </div>
                <Divider />
                {this.spotStore.currentSpotList.length === 0 ? (
                    <List>
                        <ListItem>
                            <ListItemText primary={'No Spot'} />
                        </ListItem>
                    </List>
                ) : (
                    <List>
                        {this.spotStore.currentSpotList.map((spot, index) => (
                            <ListItem
                                button={true}
                                key={spot.id}
                                onClick={this.onSelectSpot(index)}
                                selected={this.selectedIndex === spot.id}
                            >
                                <ListItemText primary={spot.name} />
                            </ListItem>
                        ))}
                    </List>
                )}
            </Paper>
        );
    }
}

const SpotListWithStyles = withStyles(styles)(SpotList);

export { SpotListWithStyles as SpotList };
