import React, { ChangeEvent, FormEvent } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { TextField, Button } from '@material-ui/core';
import { IStoreInjectProps, STORE_NAME } from '../stores';

@inject(STORE_NAME)
@observer
class CreateNewSector extends React.Component<IStoreInjectProps, {}> {
    @observable
    public sectorName: string = '';

    @observable
    public loading: boolean = false;

    @observable
    public showError: boolean = false;

    @action
    private setLoading(bool: boolean) {
        this.loading = bool;
    }

    @action
    private setShowError(bool: boolean) {
        this.showError = bool;
    }

    @action
    private setSectorName(sectorName: string) {
        this.sectorName = sectorName;
    }

    private onClose = () => {
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };
    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setLoading(true);
        this.setShowError(false);
        if (this.sectorName === '') {
            this.setLoading(false);
            this.setShowError(true);
        } else {
            await this.props[STORE_NAME]!.sectorStore.createNewSector(
                this.sectorName
            );
            this.setLoading(false);
            this.setShowError(false);
            this.props[STORE_NAME]!.appStore.closeSimpleModal();
        }
    };
    private onChangeSectorName = (e: ChangeEvent<HTMLInputElement>) => {
        this.setSectorName(e.currentTarget.value);
    };

    public render() {
        return (
            <div
                style={{
                    background: '#FAFAFA',
                    width: '300px',
                    height: '230px'
                }}
            >
                <form onSubmit={this.onSubmit}>
                    <div
                        style={{
                            width: '300px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '80px',
                            marginTop: '50px'
                        }}
                    >
                        <TextField
                            label="New Sector 이름"
                            value={this.sectorName}
                            onChange={this.onChangeSectorName}
                            autoFocus={true}
                            style={{ marginBottom: '10px', width: '250px' }}
                        />
                    </div>
                    <div
                        style={{
                            width: '300px',
                            display: 'flex',
                            height: '20px',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        {this.showError ? (
                            <div
                                style={{
                                    color: 'red'
                                }}
                            >
                                Sector 이름을 입력해주세요.
                            </div>
                        ) : null}
                    </div>
                    <div
                        style={{
                            width: '300px',
                            alignItems: 'center',
                            display: 'flex',
                            justifyContent: 'center',
                            height: '80px'
                        }}
                    >
                        <Button
                            color="primary"
                            type="submit"
                            disabled={this.loading}
                        >
                            저장
                        </Button>
                        <Button color="secondary" onClick={this.onClose}>
                            취소
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

export { CreateNewSector };
