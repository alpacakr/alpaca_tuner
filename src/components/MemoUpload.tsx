import React, { ChangeEvent, FormEvent } from 'react';
import { observable, action, computed } from 'mobx';
import { observer, inject } from 'mobx-react';
import { TextField, Button } from '@material-ui/core';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';

const styles = (theme: Theme) =>
    createStyles({
        dialogWrapper: {
            width: '400px',
            height: '400px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        textWrapper: {
            width: '400px',
            height: '50px',
            textAlign: 'center',
            fontSize: '20px'
        },
        inputWrapper: {
            width: '380px',
            margin: '10px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: '15px'
        },
        buttonWrapper: {
            width: '400px',
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        button: {
            margin: '10px'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {}

@inject(STORE_NAME)
@observer
class MemoUpload extends React.Component<IProps, {}> {
    componentWillMount() {
        if (this.spotStore.currentSpot) {
            this.setSpotMemo(this.spotStore.currentSpot.detail);
        }
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @observable
    public spotMemo: string = '';

    @observable
    public loading: boolean = false;

    @action
    private setLoading(bool: boolean) {
        this.loading = bool;
    }

    @action
    private setSpotMemo(spotMemo: string) {
        this.spotMemo = spotMemo;
    }

    private onClose = () => {
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };

    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setLoading(true);
        let payload = {
            detail: this.spotMemo
        };
        await this.spotStore.patchSpot(payload);
        this.setLoading(false);
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };

    private onChangeSpotMemo = (e: ChangeEvent<HTMLInputElement>) => {
        this.setSpotMemo(e.currentTarget.value);
    };

    public render() {
        return (
            <div className={this.props.classes.dialogWrapper}>
                <form onSubmit={this.onSubmit}>
                    <div className={this.props.classes.textWrapper}>
                        Spot 메모
                    </div>
                    <div className={this.props.classes.inputWrapper}>
                        <TextField
                            label="Spot 메모"
                            value={this.spotMemo}
                            onChange={this.onChangeSpotMemo}
                            autoFocus={true}
                            multiline
                            rows="4"
                            fullWidth
                        />
                    </div>
                    <div className={this.props.classes.buttonWrapper}>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={this.loading}
                            className={this.props.classes.button}
                        >
                            저장
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.onClose}
                            className={this.props.classes.button}
                        >
                            취소
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const MemoUploadWithStyles = withStyles(styles)(MemoUpload);
export { MemoUploadWithStyles as MemoUpload };
