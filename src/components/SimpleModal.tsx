import { Dialog } from '@material-ui/core';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { IStoreInjectProps, STORE_NAME } from '../stores';

@inject(STORE_NAME)
@observer
class SimpleModal extends React.Component<IStoreInjectProps, {}> {
    private onClose = () => {
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };

    public render() {
        const content = this.props[STORE_NAME]!.appStore.simpleModalContent;
        return (
            <Dialog
                open={this.props[STORE_NAME]!.appStore.isSimpleModalOpen}
                onClose={this.onClose}
            >
                {' '}
                {content ? content : <span />}
            </Dialog>
        );
    }
}

export { SimpleModal };
