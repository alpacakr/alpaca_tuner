import React from 'react';
import { Divider, Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { computed } from 'mobx';
import { STORE_NAME, IStoreInjectProps } from '../stores';
import { CreateNewSector } from './CreateNewSector';
import { Sector } from '../models/sector';

const styles = (theme: Theme) =>
    createStyles({
        container: {
            width: '250px',
            height: '100%',
            overflow: 'auto'
        },
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {
    onSelect?: (sector: Sector) => void;
}

@inject(STORE_NAME)
@observer
class SectorList extends React.Component<IProps, {}> {
    @computed
    get selectedIndex() {
        return this.props[STORE_NAME]!.appStore.sectorSelectedIndex;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    public onCreateNewSector = () => {
        if (this.areaStore.currentArea && this.appStore.isCreatableStatus) {
            this.appStore.openSimpleModal(<CreateNewSector />);
        }
    };

    private onSelect = (sector: Sector) => () => {
        if (this.props.onSelect) {
            this.props.onSelect(sector);
        }
    };

    public render() {
        return (
            <Paper className={this.props.classes.container}>
                <div className={this.props.classes.buttonWrapper}>
                    <Button
                        variant="contained"
                        onClick={this.onCreateNewSector}
                        onMouseOver={this.mapStore.changeAllSectorFillOpacity(
                            0.5
                        )}
                        onMouseOut={this.mapStore.changeAllSectorFillOpacity(0)}
                    >
                        New Sector
                    </Button>
                </div>
                <Divider />
                {this.sectorStore.currentSectorList.length === 0 ? (
                    <List>
                        <ListItem>
                            <ListItemText primary={'No Sector'} />
                        </ListItem>
                    </List>
                ) : (
                    <List>
                        {this.sectorStore.currentSectorList.map(
                            (sector, index) => (
                                <ListItem
                                    button={true}
                                    key={sector.id}
                                    onClick={this.onSelect(sector)}
                                    selected={this.selectedIndex === sector.id}
                                    onMouseOver={this.mapStore.changePolygonFillOpacity(
                                        index,
                                        'sector',

                                        0.5
                                    )}
                                    onMouseOut={this.mapStore.changePolygonFillOpacity(
                                        index,
                                        'sector',
                                        0
                                    )}
                                >
                                    <ListItemText primary={sector.name} />
                                </ListItem>
                            )
                        )}
                    </List>
                )}
            </Paper>
        );
    }
}

const SectorListWithStyles = withStyles(styles)(SectorList);

export { SectorListWithStyles as SectorList };
