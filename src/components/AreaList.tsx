import React from 'react';
import { Divider, Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { STORE_NAME, IStoreInjectProps } from '../stores';
import { CreateNewArea } from './CreateNewArea';
import { Area } from '../models/area';
import { computed } from 'mobx';

const styles = (theme: Theme) =>
    createStyles({
        container: {
            width: '200px',
            height: '100%'
        },
        buttonWrapper: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {
    onSelect?: (area: Area) => void;
}

@inject(STORE_NAME)
@observer
class AreaList extends React.Component<IProps, {}> {
    componentDidMount() {
        this.props[STORE_NAME]!.areaStore.getAreaList().then(() => {
            this.mapStore.loadAreaPolygon(this.areaStore.areaList);
        });
    }

    @computed
    get selectedIndex() {
        return this.props[STORE_NAME]!.appStore.areaSelectedIndex;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    get pointStore() {
        return this.props[STORE_NAME]!.pointStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    private onCreateNewArea = () => {
        if (this.appStore.isCreatableStatus) {
            this.appStore.openSimpleModal(<CreateNewArea />);
        }
    };

    private onSelect = (area: Area) => () => {
        if (this.props.onSelect) {
            this.props.onSelect(area);
        }
    };
    public render() {
        return (
            <Paper className={this.props.classes.container}>
                <div className={this.props.classes.buttonWrapper}>
                    <Button variant="contained" onClick={this.onCreateNewArea}>
                        New Area
                    </Button>
                </div>
                <Divider />
                {this.areaStore.areaList.length === 0 ? (
                    <List>
                        <ListItem>
                            <ListItemText primary={'No Area'} />
                        </ListItem>
                    </List>
                ) : (
                    <List>
                        {this.areaStore.areaList.map((area, index) => (
                            <ListItem
                                button={true}
                                key={area.id}
                                onClick={this.onSelect(area)}
                                selected={this.selectedIndex === area.id}
                                onMouseOver={this.mapStore.changePolygonFillOpacity(
                                    index,
                                    'area',
                                    0.5
                                )}
                                onMouseOut={this.mapStore.changePolygonFillOpacity(
                                    index,
                                    'area',
                                    0
                                )}
                            >
                                <ListItemText primary={area.name} />
                            </ListItem>
                        ))}
                    </List>
                )}
            </Paper>
        );
    }
}

const AreaListWithStyles = withStyles(styles)(AreaList);

export { AreaListWithStyles as AreaList };
