import React, { ChangeEvent, FormEvent } from 'react';
import { observable, action, computed } from 'mobx';
import { observer, inject } from 'mobx-react';
import { TextField, Button } from '@material-ui/core';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';

const styles = (theme: Theme) =>
    createStyles({
        dialogWrapper: {
            width: '300px',
            height: '300px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        inputWrapper: {
            width: '280px',
            margin: '10px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        errorWrapper: {
            width: '300px',
            height: '50px',
            color: 'red',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        buttonWrapper: {
            width: '300px',
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        button: {
            margin: '10px'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {}

@inject(STORE_NAME)
@observer
class ChangeSpotName extends React.Component<IProps, {}> {
    componentWillMount() {
        if (this.spotStore.currentSpot) {
            this.setSpotName(this.spotStore.currentSpot.name);
        }
    }
    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @observable
    public spotName: string = '';

    @observable
    public loading: boolean = false;

    @observable
    public showError: boolean = false;

    @action
    private setLoading(bool: boolean) {
        this.loading = bool;
    }

    @action
    private setShowError(bool: boolean) {
        this.showError = bool;
    }

    @action
    private setSpotName(spotName: string) {
        this.spotName = spotName;
    }

    private onClose = () => {
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };

    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setLoading(true);
        this.setShowError(false);

        this.setLoading(true);
        if (this.spotName === '') {
            this.setLoading(false);
            this.setShowError(true);
        } else {
            let payload = {
                name: this.spotName
            };
            await this.spotStore.patchSpot(payload);
            this.setLoading(false);
            this.setShowError(false);
            this.props[STORE_NAME]!.appStore.closeSimpleModal();
        }
    };

    private onChangeSpotName = (e: ChangeEvent<HTMLInputElement>) => {
        this.setSpotName(e.currentTarget.value);
    };

    public render() {
        return (
            <div className={this.props.classes.dialogWrapper}>
                <form onSubmit={this.onSubmit}>
                    <div className={this.props.classes.inputWrapper}>
                        <TextField
                            label="변경할 Spot의 이름"
                            value={this.spotName}
                            onChange={this.onChangeSpotName}
                            autoFocus={true}
                        />
                    </div>
                    <div className={this.props.classes.errorWrapper}>
                        {this.showError ? (
                            <div>Spot 이름을 입력해주세요.</div>
                        ) : (
                            <span />
                        )}
                    </div>
                    <div className={this.props.classes.buttonWrapper}>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={this.loading}
                            className={this.props.classes.button}
                        >
                            저장
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.onClose}
                            className={this.props.classes.button}
                        >
                            취소
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const ChangeSpotNameWithStyles = withStyles(styles)(ChangeSpotName);
export { ChangeSpotNameWithStyles as ChangeSpotName };
