import React, { FormEvent, ChangeEvent } from 'react';
import { observable, action, computed } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Button } from '@material-ui/core';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';

const styles = (theme: Theme) =>
    createStyles({
        dialogWrapper: {
            width: '600px',
            height: '650px',
            display: 'flex',
            alignItems: 'center'
        },
        textWrapper: {
            width: '600px',
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        buttonWrapper: {
            width: '600px',
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        button: {
            margin: '10px'
        },
        imageWrapper: {
            width: '600px',
            height: '400px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        img: {
            maxWidth: '580px'
        },
        errorWrapper: {
            width: '600px',
            height: '50px',
            color: 'red',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {}

@inject(STORE_NAME)
@observer
class ImageUpload extends React.Component<IProps, {}> {
    componentWillMount() {
        if (this.spotStore.currentSpot) {
            this.setImage(this.spotStore.currentSpot.photo);
        }
    }
    @observable
    private image: string = '';

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @observable
    public loading: boolean = false;

    @observable
    public showError: boolean = false;

    @action
    private setLoading(bool: boolean) {
        this.loading = bool;
    }

    @action
    private setShowError(bool: boolean) {
        this.showError = bool;
    }

    @action
    private setImage(src: string) {
        this.image = src;
    }

    @action
    private onChangeImg = (e: ChangeEvent<HTMLInputElement>) => {
        if (!e.currentTarget.files) {
            return;
        } else {
            let reader = new FileReader();
            reader.readAsDataURL(e.currentTarget.files![0]);
            reader.onload = () => {
                if (reader.result) {
                    this.setImage(String(reader.result));
                }
            };
        }
    };

    private onClose = () => {
        this.appStore.isCreatableStatus = true;
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };

    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setLoading(true);
        this.setShowError(false);

        if (this.image === '') {
            this.setLoading(false);
            this.setShowError(true);
        } else {
            let base64Data = this.image.split(',')[1];
            let payload = {
                photo: base64Data
            };
            await this.spotStore.patchSpot(payload);
            this.appStore.isCreatableStatus = true;
            this.setLoading(false);
            this.setShowError(false);
            this.props[STORE_NAME]!.appStore.closeSimpleModal();
        }
    };

    public render() {
        return (
            <div className={this.props.classes.dialogWrapper}>
                <form onSubmit={this.onSubmit}>
                    <div className={this.props.classes.textWrapper}>
                        Spot 사진
                    </div>
                    <div className={this.props.classes.imageWrapper}>
                        <img
                            className={this.props.classes.img}
                            src={this.image}
                            alt={''}
                        />
                    </div>
                    <div className={this.props.classes.buttonWrapper}>
                        <Button
                            variant="contained"
                            component="label"
                            color="primary"
                            className={this.props.classes.button}
                        >
                            사진 변경
                            <input
                                accept="image/jpeg"
                                style={{ display: 'none' }}
                                id="raised-button-file"
                                type="file"
                                onChange={this.onChangeImg}
                            />
                        </Button>
                    </div>
                    <div className={this.props.classes.errorWrapper}>
                        {this.showError ? (
                            <div>이미지를 업로드해주세요.</div>
                        ) : (
                            <span />
                        )}
                    </div>
                    <div className={this.props.classes.buttonWrapper}>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={this.loading}
                            className={this.props.classes.button}
                        >
                            저장
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.onClose}
                            className={this.props.classes.button}
                        >
                            취소
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const ImageUploadWithStyles = withStyles(styles)(ImageUpload);
export { ImageUploadWithStyles as ImageUpload };
