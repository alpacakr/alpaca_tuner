import React, { ChangeEvent, FormEvent } from 'react';
import { observable, action, computed } from 'mobx';
import { observer, inject } from 'mobx-react';
import { TextField, Button } from '@material-ui/core';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';

const styles = (theme: Theme) =>
    createStyles({
        dialogWrapper: {
            width: '300px',
            height: '300px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: 'auto'
        },
        inputWrapper: {
            width: '250px',
            margin: '10px'
        },
        errorWrapper: {
            width: '250px',
            height: '50px',
            color: 'red'
        }
    });

interface IProps extends WithStyles<typeof styles>, IStoreInjectProps {}

@inject(STORE_NAME)
@observer
class RangeSetting extends React.Component<IProps, {}> {
    componentWillMount() {
        if (this.sectorStore.currentSector) {
            this.range.min = this.sectorStore.currentSector.minNumAlpaca;
            this.range.max = this.sectorStore.currentSector.maxNumAlpaca;
        }
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @observable
    public range: {
        min: number;
        max: number;
    } = { min: 0, max: 0 };

    @observable
    public loading: boolean = false;

    @observable
    public showError: boolean = false;

    @action
    private setLoading(bool: boolean) {
        this.loading = bool;
    }

    @action
    private setShowError(bool: boolean) {
        this.showError = bool;
    }

    @action
    private setRange(min: number, max: number) {
        this.range.min = min;
        this.range.max = max;
    }

    private onClose = () => {
        this.props[STORE_NAME]!.appStore.closeSimpleModal();
    };
    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setLoading(true);
        this.setShowError(false);

        this.setLoading(true);
        if (
            this.range.max < this.range.min ||
            this.range.max < 0 ||
            this.range.min < 0
        ) {
            this.setLoading(false);
            this.setShowError(true);
        } else {
            await this.sectorStore.changeAlpacaRangeInSector(
                this.range.min,
                this.range.max
            );
            this.setLoading(false);
            this.setShowError(false);
            this.props[STORE_NAME]!.appStore.closeSimpleModal();
        }
    };
    private onChangeMin = (e: ChangeEvent<HTMLInputElement>) => {
        this.setRange(Number(e.currentTarget.value), this.range.max);
    };
    private onChangeMax = (e: ChangeEvent<HTMLInputElement>) => {
        this.setRange(this.range.min, Number(e.currentTarget.value));
    };

    public render() {
        return (
            <div className={this.props.classes.dialogWrapper}>
                <form onSubmit={this.onSubmit}>
                    <div className={this.props.classes.inputWrapper}>
                        <TextField
                            label="Min"
                            value={this.range.min}
                            onChange={this.onChangeMin}
                            type="number"
                        />
                        <TextField
                            label="Max"
                            value={this.range.max}
                            onChange={this.onChangeMax}
                            type="number"
                        />
                    </div>
                    <div className={this.props.classes.errorWrapper}>
                        {this.showError ? (
                            <div>숫자 범위를 제대로 입력해주세요.</div>
                        ) : (
                            <span />
                        )}
                    </div>
                    <div>
                        <Button
                            color="primary"
                            type="submit"
                            disabled={this.loading}
                        >
                            저장
                        </Button>
                        <Button color="secondary" onClick={this.onClose}>
                            취소
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const RangeSettingWithStyles = withStyles(styles)(RangeSetting);
export { RangeSettingWithStyles as RangeSetting };
