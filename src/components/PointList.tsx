import React from 'react';
import { Paper, Button } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { STORE_NAME, IStoreInjectProps } from '../stores';
import { computed } from 'mobx';
import markerBlack from '../img/marker_black.png';
import markerBlue from '../img/marker_blue.png';

const styles = (theme: Theme) =>
    createStyles({
        container: {
            width: '300px',
            height: '100%'
        },
        buttonWrapper: {
            height: '30px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        list: {
            maxHeight: '100%',
            overflow: 'auto'
        }
    });

type Props = WithStyles<typeof styles> & IStoreInjectProps;

@inject(STORE_NAME)
@observer
class PointList extends React.Component<Props, {}> {
    @computed
    get pointStore() {
        return this.props[STORE_NAME]!.pointStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    private onMouseOver = (index: number) => () => {
        if (this.pointStore.currentEditPointIndex !== -1) {
            return;
        }
        this.mapStore.pointCircleList[index].setIcon(markerBlue);
    };

    private onMouseOut = (index: number) => () => {
        if (this.pointStore.currentEditPointIndex !== -1) {
            return;
        }
        this.mapStore.pointCircleList[index].setIcon(markerBlack);
    };

    private onDelete = (index: number) => () => {
        if (this.pointStore.currentEditPointIndex !== -1) {
            return;
        }
        this.pointStore.deletePoint(index);
        this.mapStore.deletePointCircle(index);
    };

    public render() {
        const itemList: React.ReactNode[] = [];
        for (let i = 0; i < this.pointStore.pointList.length; i++) {
            itemList.push(
                <ListItem
                    key={i}
                    onMouseOver={this.onMouseOver(i)}
                    onMouseOut={this.onMouseOut(i)}
                >
                    <ListItemText primary={`${i}`} />
                    <div className={this.props.classes.buttonWrapper}>
                        <Button
                            variant="contained"
                            onClick={this.pointStore.onEditPoint(i)}
                        >
                            수정
                        </Button>
                        <Button variant="contained" onClick={this.onDelete(i)}>
                            삭제
                        </Button>
                    </div>
                </ListItem>
            );
        }
        return (
            <Paper className={this.props.classes.container}>
                {this.pointStore.pointList.length === 0 ? (
                    <div style={{ textAlign: 'center' }}>No Point</div>
                ) : (
                    <List className={this.props.classes.list}>
                        {itemList.map(listItem => listItem)}
                    </List>
                )}
            </Paper>
        );
    }
}

const PointListWithStyles = withStyles(styles)(PointList);

export { PointListWithStyles as PointList };
