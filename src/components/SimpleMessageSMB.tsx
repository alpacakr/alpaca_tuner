import React from 'react';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';

const styles = (theme: Theme) =>
    createStyles({
        SMBWrapper: {
            width: '100%',
            lineHeight: '90px',
            fontSize: '20px',
            textAlign: 'center'
        }
    });

interface IProps extends WithStyles<typeof styles> {
    message: string;
    color?: string;
}

class SimpleMessageSMB extends React.Component<IProps, {}> {
    public render() {
        return (
            <div
                className={this.props.classes.SMBWrapper}
                style={{ color: this.props.color }}
            >
                {`${this.props.message}`}
            </div>
        );
    }
}

const SimpleMessageSMBWithStyles = withStyles(styles)(SimpleMessageSMB);
export { SimpleMessageSMBWithStyles as SimpleMessageSMB };
