import React from 'react';
import { observer, inject } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { computed } from 'mobx';

type IProps = IStoreInjectProps;

@inject(STORE_NAME)
@observer
class SmallMenuBar extends React.Component<IProps, {}> {
    @computed
    public get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    public render() {
        return (
            <div style={{ width: '100%', height: '100px' }}>
                {this.appStore.currentSmallMenuBar}
            </div>
        );
    }
}

//const OverlayMenuBarWithStyles = withStyles(styles)(OverlayMenuBar);
export { SmallMenuBar };
