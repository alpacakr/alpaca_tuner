import React from 'react';
import Button from '@material-ui/core/Button';
import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { computed } from 'mobx';
import { isContainsPoint } from '../utils/geo';
import { PointGeojson } from '../models/spot';
import { SimpleMessageSMB } from './SimpleMessageSMB';
import { RangeBasicSMB } from './RangeBasicSMB';
import { ImageUpload } from './ImageUpload';
import { ChangeSpotName } from './ChangeSpotName';
import { MemoUpload } from './MemoUpload';

const styles = (theme: Theme) =>
    createStyles({
        textWrapper: {
            width: '200px',
            paddingTop: '10px',
            lineHeight: '20px',
            fontSize: '20px',
            textAlign: 'center'
        },
        buttonWrapper: {
            lineheight: '100px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

type IProps = WithStyles<typeof styles> & IStoreInjectProps;

@inject(STORE_NAME)
@observer
class SpotBasicSMB extends React.Component<IProps, {}> {
    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    private onEdit = () => {
        this.appStore.changeSmallMenuBar(
            <SimpleMessageSMB message={'Spot의 위치를 찍어주세요.'} />
        );
        this.mapStore.addListenerToMap('click', this.clickForEditSpotListener);
    };

    public clickForEditSpotListener = async (e: naver.maps.PointerEvent) => {
        const point: [number, number] = [e.coord['x'], e.coord['y']];
        const pointGeojson: PointGeojson = {
            type: 'Point',
            coordinates: point
        };
        if (
            isContainsPoint(
                this.sectorStore.currentSector!.polygon,
                pointGeojson
            )
        ) {
            this.mapStore.clearListenersFromMap();
            this.appStore.changeSmallMenuBar(null);
            const payload = {
                point: pointGeojson
            };
            this.appStore.changeSmallMenuBar(
                <SimpleMessageSMB message={'저장 중입니다...'} />
            );
            await this.spotStore.patchSpot(payload);
            this.appStore.changeSmallMenuBar(<SpotBasicSMBWithStyles />);
        } else {
            this.appStore.changeSmallMenuBar(
                <SimpleMessageSMB
                    message={'Sector를 벗어났습니다. 다시 찍어주세요.'}
                    color={'red'}
                />
            );
        }
    };

    private onDelete = async () => {
        this.appStore.changeSmallMenuBar(
            <SimpleMessageSMB message={'삭제 중입니다...'} />
        );
        await this.spotStore.deleteSpot(this.spotStore.currentSpot);
        this.appStore.changeSmallMenuBar(<RangeBasicSMB />);
    };

    private onImage = () => {
        this.appStore.openSimpleModal(<ImageUpload />);
    };

    private onChange = () => {
        this.appStore.openSimpleModal(<ChangeSpotName />);
    };

    private onMemo = () => {
        this.appStore.openSimpleModal(<MemoUpload />);
    };

    public render() {
        return (
            <div style={{ display: 'flex' }}>
                <div className={this.props.classes.textWrapper}>
                    {`${this.areaStore.currentArea!.name}`}
                    <br />
                    {this.sectorStore.currentSector
                        ? `${this.sectorStore.currentSector.name}`
                        : ''}
                    <br />
                    {this.spotStore.currentSpot
                        ? `${this.spotStore.currentSpot.name}`
                        : ''}
                </div>
                <div className={this.props.classes.buttonWrapper}>
                    <Button
                        style={{ margin: '10px' }}
                        variant="contained"
                        onClick={this.onChange}
                    >
                        이름 변경
                    </Button>
                    <Button
                        style={{ margin: '10px' }}
                        variant="contained"
                        onClick={this.onEdit}
                    >
                        Spot 수정
                    </Button>
                    <Button
                        style={{ margin: '10px' }}
                        variant="contained"
                        onClick={this.onImage}
                    >
                        사진
                    </Button>
                    <Button
                        style={{ margin: '10px' }}
                        variant="contained"
                        onClick={this.onMemo}
                    >
                        메모
                    </Button>
                    <Button
                        style={{ margin: '10px' }}
                        variant="contained"
                        onClick={this.onDelete}
                    >
                        삭제
                    </Button>
                </div>
            </div>
        );
    }
}

const SpotBasicSMBWithStyles = withStyles(styles)(SpotBasicSMB);
export { SpotBasicSMBWithStyles as SpotBasicSMB };
