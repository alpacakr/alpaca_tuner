import {
    createStyles,
    Theme,
    withStyles,
    WithStyles
} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { inject, observer } from 'mobx-react';
import React, { ChangeEvent, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { LoadingButton } from '../components/LoadingButton';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { sleep } from '../utils/promise';
import { action, observable } from 'mobx';

interface IState {
    email: string;
    password: string;
    loading: boolean;
    showError: boolean;
}

const styles = (theme: Theme) =>
    createStyles({
        container: {
            position: 'relative',
            width: '100vw',
            height: '100vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        imageDiv: {
            background: `url("${
                process.env.PUBLIC_URL
            }/forest.jpg") no-repeat center center fixed`,
            backgroundSize: 'cover',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            filter: 'blur(2px)'
        },
        textInputWrapper: {
            width: '300px',
            background: 'white',
            zIndex: 1,
            padding: '20px',
            borderRadius: '10px'
        },
        errorText: {
            color: theme.palette.secondary.main,
            fontSize: '0.8em',
            textAlign: 'center',
            marginBottom: '20px'
        }
    });

type Props = IStoreInjectProps & WithStyles<typeof styles>;

@inject(STORE_NAME)
@observer
class Login extends React.Component<Props & RouteComponentProps, IState> {
    @observable
    public readonly iState: IState = {
        email: '',
        password: '',
        loading: false,
        showError: false
    };

    @action
    private setEmail(email: string) {
        this.iState.email = email;
    }

    @action
    private setPassword(password: string) {
        this.iState.password = password;
    }

    @action
    private setLoading(bool: boolean) {
        this.iState.loading = bool;
    }

    @action
    private setShowError(bool: boolean) {
        this.iState.showError = bool;
    }

    private onChangeEmail = (e: ChangeEvent<HTMLInputElement>) => {
        this.setEmail(e.currentTarget.value);
    };

    private onChangePassword = (e: ChangeEvent<HTMLInputElement>) => {
        this.setPassword(e.currentTarget.value);
    };

    private onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        this.setLoading(true);
        this.setShowError(false);

        await sleep(200);
        const { email, password } = this.iState;
        if (email === '' || password === '') {
            this.setLoading(false);
            this.setShowError(true);
        } else {
            const result = await this.props[
                STORE_NAME
            ]!.userStore.requestStaffEmailLogin(email, password);
            if (result) {
                this.props.history.replace('/');
            } else {
                this.setLoading(false);
                this.setShowError(true);
            }
        }
    };

    public render() {
        const { loading, showError } = this.iState;
        return (
            <div className={this.props.classes.container}>
                <div className={this.props.classes.imageDiv} />
                <div className={this.props.classes.textInputWrapper}>
                    <form onSubmit={this.onSubmit}>
                        <TextField
                            label="이메일"
                            value={this.iState.email}
                            onChange={this.onChangeEmail}
                            fullWidth={true}
                            autoFocus={true}
                            style={{ marginBottom: '10px' }}
                        />
                        <TextField
                            style={{ margin: '20px 0' }}
                            label="비밀번호"
                            value={this.iState.password}
                            onChange={this.onChangePassword}
                            fullWidth={true}
                            type="password"
                        />
                        {showError ? (
                            <div className={this.props.classes.errorText}>
                                이메일 및 비밀번호가 올바르지 않습니다.
                            </div>
                        ) : null}
                        <LoadingButton
                            loading={loading}
                            color="primary"
                            fullWidth={true}
                            variant="contained"
                            type="submit"
                            disabled={loading}
                        >
                            로그인
                        </LoadingButton>
                    </form>
                </div>
            </div>
        );
    }
}

const LoginWithStyles = withStyles(styles)(Login);

export { LoginWithStyles as Login };
