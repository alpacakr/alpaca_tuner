import React from 'react';
import { Map } from '../components/Map';
import { AreaList } from '../components/AreaList';
import { SectorList } from '../components/SectorList';
import { SmallMenuBar } from '../components/SmallMenuBar';
import { PointList } from '../components/PointList';
import { Area } from '../models/area';
import { inject } from 'mobx-react';
import { STORE_NAME, IStoreInjectProps } from '../stores';
import { computed } from 'mobx';
import { AreaBasicSMB } from '../components/AreaBasicSMB';
import { Sector } from '../models/sector';
import { SectorBasicSMB } from '../components/SectorBasicSMB';

@inject(STORE_NAME)
class AreaEditor extends React.Component<IStoreInjectProps, {}> {
    componentWillUnmount() {
        this.props[STORE_NAME]!.reset();
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    private onSelectArea = (area: Area) => {
        this.areaStore.updateCurrentArea(area);
        this.appStore.changeSmallMenuBar(<AreaBasicSMB />);
        this.appStore.isCreatableStatus = true;
    };

    private onSelectSector = (sector: Sector) => {
        this.sectorStore.updateCurrentSector(sector);
        this.appStore.changeSmallMenuBar(<SectorBasicSMB />);
        this.appStore.isCreatableStatus = true;
    };

    public render() {
        return (
            <div style={{ width: '100%', height: '100%', display: 'flex' }}>
                <AreaList onSelect={this.onSelectArea} />
                <SectorList onSelect={this.onSelectSector} />
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        flexDirection: 'column'
                    }}
                >
                    <SmallMenuBar />
                    <Map
                        mapStyle={{ width: '100%', height: '100%' }}
                        mapOption={{
                            center: new window.naver.maps.LatLng(
                                36.370569,
                                127.361278
                            )
                        }}
                    />
                </div>
                <PointList />
            </div>
        );
    }
}

export { AreaEditor };
