import React from 'react';
import { Map } from '../components/Map';
import { AreaList } from '../components/AreaList';
import { SectorList } from '../components/SectorList';
import { SmallMenuBar } from '../components/SmallMenuBar';
import { inject } from 'mobx-react';
import { STORE_NAME, IStoreInjectProps } from '../stores';
import { Sector } from '../models/sector';
import { computed } from 'mobx';
import { Area } from '../models/area';
import { RangeBasicSMB } from '../components/RangeBasicSMB';
import { SpotList } from '../components/SpotList';

interface IProps extends IStoreInjectProps {}

@inject(STORE_NAME)
class SectorSetting extends React.Component<IProps, {}> {
    componentWillUnmount() {
        this.props[STORE_NAME]!.reset();
    }

    @computed
    get areaStore() {
        return this.props[STORE_NAME]!.areaStore;
    }

    @computed
    get sectorStore() {
        return this.props[STORE_NAME]!.sectorStore;
    }

    @computed
    get appStore() {
        return this.props[STORE_NAME]!.appStore;
    }

    @computed
    get spotStore() {
        return this.props[STORE_NAME]!.spotStore;
    }

    @computed
    get mapStore() {
        return this.props[STORE_NAME]!.mapStore;
    }

    private onSelectArea = (area: Area) => {
        this.areaStore.updateCurrentArea(area);
        this.appStore.isCreatableStatus = true;
    };

    private onSelectSector = (sector: Sector) => {
        this.sectorStore.updateCurrentSector(sector);
        this.mapStore.loadSpotMarker();
        this.appStore.changeSmallMenuBar(<RangeBasicSMB />);
        this.appStore.isCreatableStatus = true;
    };

    public render() {
        return (
            <div style={{ width: '100%', height: '100%', display: 'flex' }}>
                <AreaList onSelect={this.onSelectArea} />
                <SectorList onSelect={this.onSelectSector} />
                <SpotList />
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        flexDirection: 'column'
                    }}
                >
                    <SmallMenuBar />
                    <Map
                        mapStyle={{ width: '100%', height: '100%' }}
                        mapOption={{
                            center: new window.naver.maps.LatLng(
                                36.370569,
                                127.361278
                            )
                        }}
                    />
                </div>
            </div>
        );
    }
}

export { SectorSetting };
