import CircularProgress from '@material-ui/core/CircularProgress';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { observable, action } from 'mobx';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import { Route, Switch, withRouter } from 'react-router-dom';
import { IStoreInjectProps, STORE_NAME } from '../stores';
import { Login } from '../pages/Login';
import { AreaEditor } from '../pages/AreaEditor';
import { SectorSetting } from '../pages/SectorSetting';
import { MenuBar } from '../components/MenuBar';
import { SimpleModal } from '../components/SimpleModal';

const styles = createStyles({
    loadingWrapper: {
        width: '100vw',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    desktopApp: {
        minWidth: '1366px',
        minHeight: '768px',
        width: '100vw',
        height: '100vh',
        display: 'flex'
    },
    desktopAppContent: {
        flex: 1
    }
});

type Props = IStoreInjectProps &
    RouteComponentProps &
    WithStyles<typeof styles>;

interface IState {
    loading: boolean;
}

@inject(STORE_NAME)
@observer
class AppRouter extends React.Component<Props, IState> {
    @observable
    public readonly iState: IState = {
        loading: true
    };

    @action
    private setLoading(bool: boolean) {
        this.iState.loading = bool;
    }

    public componentDidMount() {
        this.requestTokenLogin();
    }

    private async requestTokenLogin() {
        const result = await this.props[
            STORE_NAME
        ]!.userStore.requestTokenLogin();
        if (!result) {
            this.props.history.replace('/login');
        }
        this.setLoading(false);
    }

    public render() {
        if (this.iState.loading) {
            return (
                <div className={this.props.classes.loadingWrapper}>
                    <CircularProgress />
                </div>
            );
        }
        const loginUser = this.props[STORE_NAME]!.userStore.loginUser;
        if (loginUser === null) {
            return <Route path="/login" component={Login} />;
        }
        return (
            <React.Fragment>
                <Switch>
                    <Route>
                        <div className={this.props.classes.desktopApp}>
                            <MenuBar />
                            <div
                                className={this.props.classes.desktopAppContent}
                            >
                                <Switch>
                                    <Route
                                        exact={true}
                                        path="/"
                                        component={AreaEditor}
                                    />
                                    <Route
                                        path="/sector-setting"
                                        component={SectorSetting}
                                    />
                                </Switch>
                            </div>
                        </div>
                    </Route>
                </Switch>
                <SimpleModal />
            </React.Fragment>
        );
    }
}

const AppRouterWithRouterStyles = withRouter(withStyles(styles)(AppRouter));

export { AppRouterWithRouterStyles as AppRouter };
