import { Provider } from 'mobx-react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AppRouter } from './routers/AppRouter';
import { RootStore } from './stores';

const store = new RootStore();

class App extends React.Component<{}, {}> {
    public render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <AppRouter />
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
