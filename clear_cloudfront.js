const CloudFront = require('aws-sdk/clients/cloudfront');

const cloudfront = new CloudFront({ apiVersion: '2018-06-18' });

cloudfront.createInvalidation(
    {
        DistributionId: 'EDFD63N1CCTVV',
        InvalidationBatch: {
            CallerReference: Math.floor(new Date() / 1000).toString(),
            Paths: {
                Quantity: 1,
                Items: ['/*']
            }
        }
    },
    (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log(
                `Successfully create invalidation. Current status is ${
                    data.Invalidation.Status
                }.`
            );
        }
    }
);
