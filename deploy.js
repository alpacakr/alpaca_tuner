const S3 = require('aws-sdk/clients/s3');
const path = require('path');
const fs = require('fs');
const mime = require('mime-types');

const uploadDir = function(buildDirectoryPath, bucketName) {
    const s3 = new S3({ apiVersion: '2006-03-01', region: 'ap-northeast-2' });

    function walkSync(currentDirPath, callback) {
        fs.readdirSync(currentDirPath).forEach(function(name) {
            const filePath = path.join(currentDirPath, name);
            const stat = fs.statSync(filePath);
            if (stat.isFile()) {
                callback(filePath);
            } else if (stat.isDirectory()) {
                walkSync(filePath, callback);
            }
        });
    }

    walkSync(buildDirectoryPath, function(filePath) {
        const bucketPath = filePath.substring(buildDirectoryPath.length + 1);
        const contentType =
            mime.lookup(bucketPath) || 'application/octet-stream';
        const params = {
            Bucket: bucketName,
            Key: bucketPath,
            Body: fs.readFileSync(filePath),
            ContentType: contentType
        };
        s3.putObject(params, function(err, data) {
            if (err) {
                console.log(err);
            } else {
                console.log(
                    'Successfully uploaded ' + bucketPath + ' to ' + bucketName
                );
            }
        });
    });
};

uploadDir(`${__dirname}/build`, 'alpaca-tuner');
